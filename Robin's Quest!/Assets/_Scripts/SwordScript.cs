﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordScript : MonoBehaviour
{
    public GameObject player;
    public CircleCollider2D scabbard;

    enum SwordState { Attacking, Returning, Sheathed };
    SwordState currentState = SwordState.Sheathed;

    int dmg = 1;

    float lerpSpeed = .05f;
    float lerpTimer = .00f;

    Vector2 home;
    Vector2 destination;
    Vector2 location;

    void FixedUpdate()
    {
        home = player.transform.position;
        location = new Vector2(transform.position.x, transform.position.y);

        if (currentState == SwordState.Attacking)
        {
            this.gameObject.transform.position = Vector2.Lerp(location, destination, lerpSpeed);
            lerpTimer += 0.1f;
            if (lerpTimer >= 4f) // <------------
            {
                currentState = SwordState.Returning;
                lerpTimer = 0f;
            }
        }
        if (currentState == SwordState.Returning)
        {
            lerpSpeed += .005f;
            this.gameObject.transform.position = Vector2.Lerp(transform.position, home, lerpSpeed);
        }
        if(currentState == SwordState.Sheathed)
        {
            lerpTimer += 0.1f;
            if(lerpTimer >= 2)
            {
                ResetSword();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (currentState != SwordState.Attacking)
        {
            if (collision == player.GetComponentInChildren<CircleCollider2D>()) 
            {
                ResetSword();
            }
        }

        if ((collision.tag == "Enemies") &&
            (collision.GetComponent<Enemies>().IsAlive()))
        {
            collision.GetComponent<Enemies>().TakeDamage(dmg);
        }
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (currentState != SwordState.Attacking)
        {
            if (collision == player.GetComponentInChildren<CircleCollider2D>())
            {
                ResetSword();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.collider.tag == "Walls") ||
            (collision.collider.tag == "NPCs" ))
        {
            currentState = SwordState.Returning;
            lerpTimer = 0f;
        }
    }

    public void Attack(string dir)
    {
        currentState = SwordState.Attacking;

        this.gameObject.transform.position = player.transform.position;
        home = this.gameObject.transform.position;

        switch (dir)
        {
            case "N":
                destination = home + new Vector2(0, 5);
                break;
            case "S":
                destination = home + new Vector2(0, -5);
                break;
            case "E":
                destination = home + new Vector2(5, 0);
                break;
            case "W":
                destination = home + new Vector2(-5, 0);
                break;
            default:
                break;
        }
    }

    public void ResetSword()
    {
        currentState = SwordState.Sheathed;
        player.GetComponent<PlayerScript>().attacking = false;
        lerpTimer = 0f;
        lerpSpeed = .05f;
        Destroy(this.gameObject);
    }

    public void ReturnSword()
    {
        currentState = SwordState.Sheathed;
        lerpTimer = 0f;
    }
}
