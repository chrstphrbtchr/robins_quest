﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Dialogue : MonoBehaviour 
{
    // textbox
    public bool messaging;

    // objects
    public PlayerMovement player;
    public Camera cam;
    public CanvasGroup txtBox;
    public SpriteRenderer border;
    public GameObject arrow;
    public GameObject letter;
    List<GameObject> letters = new List<GameObject>();

    // tag handling.
    bool inTag;
    bool outTag;

    bool colorTag;
    bool timeTag;
    bool keyTag;
    bool pitchTag;

    string colorStr = "";
    string timeStr = "";
    string pitchStr = "";

    // sprite handler
    public Dictionary<string, Sprite> stringToSprite = new Dictionary<string, Sprite>();

    // letter stuff
    Vector2 defaultTextPos;
    Vector2 currentTextPos;
    Color defaultColor = new Color(0.835f, 1, 0.91f, 1);
    Color keyItem = new Color(1f, 0.39215686274f, 0.39215686274f, 1f);
    Color hintTxt = Color.yellow;                 //
    Color badTxt = Color.red; // make spookier?   //
    Color whiteTxt = Color.white;                 //
    Color blackTxt = Color.black;                 //
    Color currentColor;
    float defaultTypeTime = .05f;
    static public float currentTypeTime; 
    float defaultPitch = 1f;
    float currentPitch;
    AudioSource voice;
    public AudioClip chirp;
    public AudioClip dog;
    public AudioClip evil;
    public AudioClip npcs;
    bool shake;
    bool wave;

    // sprite alphabet
    public Sprite t00;
    public Sprite t01;
    public Sprite t02;
    public Sprite t03;
    public Sprite t04;
    public Sprite t05;
    public Sprite t06;
    public Sprite t07;
    public Sprite t08;
    public Sprite t09;
    public Sprite t10;
    public Sprite t11;
    public Sprite t12;
    public Sprite t13;
    public Sprite t14;
    public Sprite t15;
    public Sprite t16;
    public Sprite t17;
    public Sprite t18;
    public Sprite t19;
    public Sprite t20;
    public Sprite t21;
    public Sprite t22;
    public Sprite t23;
    public Sprite t24;
    public Sprite t25;
    public Sprite t26;
    public Sprite t27;
    public Sprite t28;
    public Sprite t29;
    public Sprite t30;
    public Sprite t31;
    public Sprite t32;
    public Sprite t33;
    public Sprite t34;
    public Sprite t35;
    public Sprite t36;
    public Sprite t37;
    public Sprite t38;
    public Sprite t39;
    public Sprite t40;
    public Sprite t41;
    public Sprite t42;
    public Sprite t43;
    public Sprite t44;
    public Sprite t45;
    public Sprite t46;
    public Sprite t47;
    public Sprite t48;
    public Sprite t49;
    public Sprite t50;
    public Sprite t51;
    public Sprite t52;
    public Sprite t53;
    public Sprite t54;
    public Sprite t55;
    public Sprite t56;
    public Sprite t57;
    public Sprite t58;
    public Sprite t59;
    public Sprite t60;
    public Sprite t61;
    public Sprite t62;
    public Sprite t63;
    public Sprite t64;
    public Sprite t65;
    public Sprite t66;
    public Sprite t67;
    public Sprite t68;
    public Sprite t69;
    public Sprite t70;
    public Sprite t71;
    public Sprite t72;
    public Sprite t73;
    public Sprite t74;
    public Sprite t75;
    public Sprite t76;
    public Sprite t77;
    public Sprite t78;
    public Sprite t79;
    public Sprite t80;
    public Sprite t81;
    public Sprite t82;
    public Sprite t83;
    public Sprite t84;
    public Sprite t85;
    public Sprite t86;
    public Sprite t87;
    public Sprite t88;
    public Sprite t89;
    public Sprite t90;
    public Sprite t91;
    public Sprite t92;
    public Sprite t93;
    public Sprite t94;
    public Sprite t95;
    public Sprite t96;

    // keyboard sprite objects
    public GameObject kW;
    public GameObject kA;
    public GameObject kS;
    public GameObject kD;
    public GameObject kEnter;
    public GameObject kEsc;
    public GameObject kShift;
    public GameObject kSpace;
    public GameObject kTab;


    void Start()
    {
        defaultTextPos = new Vector2(cam.transform.position.x - 6.075f, 
            cam.transform.position.y - 1.5f);
        stringToSprite.Add("´", t00);
        stringToSprite.Add("!", t01);
        stringToSprite.Add("\"", t02);
        stringToSprite.Add("#", t03);
        stringToSprite.Add("$", t04);
        stringToSprite.Add("%", t05);
        stringToSprite.Add("&", t06);
        stringToSprite.Add("'", t07);
        stringToSprite.Add("(", t08);
        stringToSprite.Add(")", t09);
        stringToSprite.Add("*", t10);
        stringToSprite.Add("+", t11);
        stringToSprite.Add(",", t12);
        stringToSprite.Add("-", t13);
        stringToSprite.Add(".", t14);
        stringToSprite.Add("/", t15);
        stringToSprite.Add("0", t16);
        stringToSprite.Add("1", t17);
        stringToSprite.Add("2", t18);
        stringToSprite.Add("3", t19);
        stringToSprite.Add("4", t20);
        stringToSprite.Add("5", t21);
        stringToSprite.Add("6", t22);
        stringToSprite.Add("7", t23);
        stringToSprite.Add("8", t24);
        stringToSprite.Add("9", t25);
        stringToSprite.Add(":", t26);
        stringToSprite.Add(";", t27);
        stringToSprite.Add("<", t28);
        stringToSprite.Add("=", t29);
        stringToSprite.Add(">", t30);
        stringToSprite.Add("?", t31);
        stringToSprite.Add("@", t32);
        stringToSprite.Add("A", t33);
        stringToSprite.Add("B", t34);
        stringToSprite.Add("C", t35);
        stringToSprite.Add("D", t36);
        stringToSprite.Add("E", t37);
        stringToSprite.Add("F", t38);
        stringToSprite.Add("G", t39);
        stringToSprite.Add("H", t40);
        stringToSprite.Add("I", t41);
        stringToSprite.Add("J", t42);
        stringToSprite.Add("K", t43);
        stringToSprite.Add("L", t44);
        stringToSprite.Add("M", t45);
        stringToSprite.Add("N", t46);
        stringToSprite.Add("O", t47);
        stringToSprite.Add("P", t48);
        stringToSprite.Add("Q", t49);
        stringToSprite.Add("R", t50);
        stringToSprite.Add("S", t51);
        stringToSprite.Add("T", t52);
        stringToSprite.Add("U", t53);
        stringToSprite.Add("V", t54);
        stringToSprite.Add("W", t55);
        stringToSprite.Add("X", t56);
        stringToSprite.Add("Y", t57);
        stringToSprite.Add("Z", t58);
        stringToSprite.Add("[", t59);
        stringToSprite.Add("\\", t60);
        stringToSprite.Add("]", t61);
        stringToSprite.Add("^", t62);
        stringToSprite.Add("_", t63);
        stringToSprite.Add("`", t64);
        stringToSprite.Add("a", t65);
        stringToSprite.Add("b", t66);
        stringToSprite.Add("c", t67);
        stringToSprite.Add("d", t68);
        stringToSprite.Add("e", t69);
        stringToSprite.Add("f", t70);
        stringToSprite.Add("g", t71);
        stringToSprite.Add("h", t72);
        stringToSprite.Add("i", t73);
        stringToSprite.Add("j", t74);
        stringToSprite.Add("k", t75);
        stringToSprite.Add("l", t76);
        stringToSprite.Add("m", t77);
        stringToSprite.Add("n", t78);
        stringToSprite.Add("o", t79);
        stringToSprite.Add("p", t80);
        stringToSprite.Add("q", t81);
        stringToSprite.Add("r", t82);
        stringToSprite.Add("s", t83);
        stringToSprite.Add("t", t84);
        stringToSprite.Add("u", t85);
        stringToSprite.Add("v", t86);
        stringToSprite.Add("w", t87);
        stringToSprite.Add("x", t88);
        stringToSprite.Add("y", t89);
        stringToSprite.Add("z", t90);
        stringToSprite.Add("{", t91);
        stringToSprite.Add("|", t92);
        stringToSprite.Add("}", t93);
        stringToSprite.Add("~", t94);
        stringToSprite.Add("¦", t95);
        stringToSprite.Add(" ", t96);

        txtBox.alpha = 0f;
        border.color = new Color(1, 1, 1, 0);
        currentColor = defaultColor;
        currentPitch = defaultPitch;
        currentTextPos = defaultTextPos;
        currentTypeTime = defaultTypeTime;
    }

    void Update()
    {
        
    } 

    public void StartConversation(List<List<string>> l)
    {
        txtBox.alpha = 1f;
        border.color = new Color(1, 1, 1, 1);
        StartCoroutine(Conversation(l));
        PlayerMovement.frozen = true;
    }

    public void UpdateCamera(Vector3 t)
    {
        defaultTextPos = new Vector2(t.x - 6.075f,
            t.y - 1.5f);
    }

    public IEnumerator Conversation(List<List<string>> l)
    {
        messaging = true;
        currentTextPos = defaultTextPos;
        for (int i = 0; i < l.Count; i++)
        {
            // all dialogue for this character
            for (int s = 0; s < l[i].Count; s++)
            {
                // each box of text
                foreach(char c in l[i][s])
                {
                    if (c.ToString() == "<")
                    {
                        inTag = true;
                        yield return null;
                    }

                    if (!inTag)
                    {
                        GetComponent<AudioSource>().PlayOneShot(chirp,
                            UnityEngine.Random.Range(0.5f, 1f));
                        GetComponent<AudioSource>().pitch = UnityEngine.Random.Range(0.95f, 1.125f);
                        GameObject g = Instantiate(letter, currentTextPos, transform.rotation);
                        g.GetComponent<SpriteRenderer>().sprite = stringToSprite[c.ToString()];
                        g.GetComponent<SpriteRenderer>().color = currentColor;
                        g.GetComponent<LetterScript>().shake = shake;
                        g.GetComponent<LetterScript>().wave = wave;
                        currentTextPos.x += g.transform.localScale.x;
                        letters.Add(g);

                        yield return new WaitForSecondsRealtime(currentTypeTime);
                    }
                    else
                    {
                        if (c.ToString() != "/")
                        {
                            if (c.ToString() == ">")
                            {
                                // if in color, color = rgba  strings to float
                                if (colorTag)
                                {
                                    if (outTag)
                                    {
                                        currentColor = defaultColor;
                                        outTag = false;
                                        colorTag = false;
                                        yield return null;
                                    }
                                    else
                                    {
                                        yield return null;
                                    }
                                }
                                // if in pitch, pitch = pitch string  to float
                                if (pitchTag)
                                {
                                    if (!outTag)
                                    {
                                        currentPitch = float.Parse(pitchStr);
                                        pitchStr = "";
                                    }
                                    else
                                    {
                                        currentPitch = defaultPitch;
                                        outTag = false;
                                        pitchTag = false;
                                        yield return null;
                                    }
                                }
                                // if in time , time  = time  string  to float
                                if (timeTag)
                                {
                                    if (!outTag)
                                    {
                                        currentTypeTime = float.Parse(timeStr);
                                        timeStr = "";
                                    }
                                    else
                                    {
                                        currentTypeTime = defaultTypeTime;
                                        outTag = false;
                                        timeTag = false;
                                        yield return null;
                                    }
                                }
                                if ((shake) && (outTag))
                                {
                                    shake = false;
                                    outTag = false;
                                }

                                if ((wave) && (outTag))
                                {
                                    wave = false;
                                    outTag = false;
                                }                              
                                inTag = false;
                                yield return null;
                            }
                            else if (c.ToString() == "w")
                            {
                                wave = true;
                            }
                            else if (c.ToString() == "s")
                            {
                                shake = true;
                            }
                            else if (c.ToString() == "p")
                            {
                                pitchTag = true;
                            }
                            else if (c.ToString() == "t")
                            {
                                timeTag = true;
                            }
                            else if (c.ToString() == "c")
                            {
                                colorTag = true;
                            }
                            else if (c.ToString() == "k")
                            {
                                keyTag = true;
                                yield return null;
                            }
                            else if ((colorTag) && (c.ToString() != ">"))
                            {
                                if (c.ToString() == "i")
                                {
                                    currentColor = keyItem;
                                    colorTag = false;
                                    yield return null;
                                }
                                else if (c.ToString() == "e")
                                {
                                    // evil text
                                }
                                else if (c.ToString() == "h")
                                {
                                    // hint text
                                }
                                else
                                {
                                    colorTag = false;
                                    yield return null;
                                }
                            }
                            else if ((keyTag) && (c.ToString() != ">"))
                            {
                                GetComponent<AudioSource>().PlayOneShot(chirp,
                                    UnityEngine.Random.Range(0.5f, 1f));
                                GetComponent<AudioSource>().pitch = 
                                    UnityEngine.Random.Range(0.95f, 1.125f);

                                if (c.ToString() == "W")
                                {
                                    GameObject g = Instantiate(kW,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "A")
                                {
                                    GameObject g = Instantiate(kA,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "S")
                                {
                                    GameObject g = Instantiate(kS,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "D")
                                {
                                    GameObject g = Instantiate(kD,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "E")
                                {
                                    // enter
                                    currentTextPos.x += 0.5f;
                                    GameObject g = Instantiate(kEnter,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += (0.25f + g.transform.localScale.x);
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "X")
                                {
                                    // esc
                                    GameObject g = Instantiate(kEsc,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "T")
                                {
                                    // tab
                                    currentTextPos.x += 0.25f;
                                    GameObject g = Instantiate(kTab,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += g.transform.localScale.x;
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == "R")
                                {
                                    // shift
                                    currentTextPos.x += 0.25f;
                                    GameObject g = Instantiate(kShift,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += (0.25f + g.transform.localScale.x);
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else if (c.ToString() == " ")
                                {
                                    // space
                                    currentTextPos.x += 0.75f;
                                    GameObject g = Instantiate(kSpace,
                                        currentTextPos, transform.rotation);
                                    currentTextPos.x += (0.75f + g.transform.localScale.x);
                                    letters.Add(g);
                                    keyTag = false;
                                    yield return new WaitForSecondsRealtime(currentTypeTime);
                                }
                                else
                                {
                                    keyTag = false;
                                    yield return null;
                                }
                            }
                            else
                            {
                                Console.Write("Unknown Tag.");
                            }
                        }
                        else
                        {
                            outTag = true;
                            yield return null;
                        }

                        if ((colorTag) && (!Char.IsLetter(c)))
                        {
                            colorStr = colorStr + c.ToString();
                            yield return null;
                        }

                        if ((pitchTag) && (!Char.IsLetter(c)))
                        {
                            pitchStr = pitchStr + c.ToString();
                            yield return null;
                        }

                        if ((timeTag) && (!Char.IsLetter(c)))
                        {
                            timeStr = timeStr + c.ToString();
                            yield return null;
                        }
                    }
                }
                currentTextPos.y -= 0.75f;
                currentTextPos.x = defaultTextPos.x;
            }
            if((i+1 < l.Count()))
            {
                GameObject a = Instantiate(arrow,
                    new Vector3(cam.transform.position.x + 6.25f, cam.transform.position.y - 3.25f, 0),
                    Quaternion.AngleAxis(-90, Vector3.forward));
                letters.Add(a);
            }
            yield return StartCoroutine(WaitForInput(KeyCode.RightShift));
            foreach (GameObject g in letters)
            {
                Destroy(g);
            }
            currentTextPos = defaultTextPos;
        }
        messaging = false;
        border.color = new Color(1, 1, 1, 0);
        txtBox.alpha = 0f;
        PlayerMovement.frozen = false;
        PlayerScript.heyListen = false;
        player.rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        currentTypeTime = defaultTypeTime;
        yield return null;
    }

    IEnumerator WaitForInput(KeyCode k)
    {
        while (!Input.GetKeyDown(k))
        {
            yield return null;
        } 
    }
}
