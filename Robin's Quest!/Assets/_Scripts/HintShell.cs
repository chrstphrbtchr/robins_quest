﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintShell : MonoBehaviour
{
    List<List<string>> hints = new List<List<string>>();
    public Dialogue dialogue;

    public void Hintmaster()
    {
        if (PlayerScript.sword)
        {
            if (PlayerScript.hammer)
            {
                if (PlayerScript.shovel)
                {
                    if (PlayerScript.mush)
                    {
                        if (PlayerScript.glove)
                        {
                            if (PlayerScript.gem)
                            {
                                if (PlayerScript.boots)
                                {
                                    if (PlayerScript.ring)
                                    {
                                        if (PlayerScript.scroll)
                                        {
                                            if (PlayerScript.floppy)
                                            {
                                                hints.Clear();
                                                List<string> a = new List<string>
                                                {
                                                    "My quest is complete.",
                                                    "But yours is not yet over.",
                                                    "Farewell, & good luck."
                                                };
                                                hints.Add(a);
                                            }
                                            else
                                            {
                                                hints.Clear();
                                                List<string> b = new List<string>
                                                {
                                                    "Hidden among trees,",
                                                    "a mysterious relic.",
                                                    "Not lost, yet not found."
                                                };
                                                hints.Add(b);
                                            }
                                        }
                                        else
                                        {
                                            hints.Clear();
                                            List<string> d = new List<string>
                                            {
                                                "Those who know the path",
                                                "are not in need of a map,",
                                                "but others must search."
                                            };
                                            hints.Add(d);
                                        }
                                    }
                                    else
                                    {
                                        if (PlayerScript.scroll)
                                        {
                                            hints.Clear();
                                            List<string> e = new List<string>
                                            {
                                                "Translating this text",
                                                "requires a leap of faith",
                                                "across a chasm."
                                            };
                                            hints.Add(e);
                                        }
                                        else
                                        {
                                            hints.Clear();
                                            List<string> f = new List<string>
                                            {
                                                "Two parts of a whole:",
                                                "one above and one below",
                                                "show the way to go."
                                            };
                                            hints.Add(f);
                                        }
                                    }
                                }
                                else
                                {
                                    hints.Clear();
                                    List<string> g = new List<string>
                                    {
                                        "Safe passage across.",
                                        "A bargain at any price",
                                        "for those who explore."
                                    };
                                    hints.Add(g);
                                }
                            }
                            else
                            {
                                hints.Clear();
                                List<string> h = new List<string>
                                {
                                    "Your stone-moving glove!",
                                    "Matchless, save for one weakness:",
                                    "Capitalism."
                                };
                                hints.Add(h);
                            }
                        }
                        else
                        {
                            hints.Clear();
                            List<string> i = new List<string>
                            {
                                "Underneath a stone,",
                                "the vegetarian dog",
                                "may help you along."
                            };
                            hints.Add(i);
                        }
                    }
                    else
                    {
                        hints.Clear();
                        List<string> j = new List<string>
                        {
                            "Rare delicacies",
                            "hidden within the forest",
                            "can open new doors."
                        };
                        hints.Add(j);
                    }
                }
                else
                {
                    hints.Clear();
                    List<string> k = new List<string>
                    {
                        "The dead have no need",
                        "of that which has dug their grave.",
                        "Yet the sands still shift."
                    };
                    hints.Add(k);
                }
            }
            else
            {
                hints.Clear();
                List<string> l = new List<string>
                {
                    "An overgrown blob,",
                    "inside an ancient dungeon.",
                    "Thus begins your quest."
                };
                hints.Add(l);
            }
        }
        else
        {
            hints.Clear();
            List<string> m = new List<string>
            {
                "How did you get here?",
                "You should have the sword by now.",
                "This must be a bug."
            };
            hints.Add(m);
        }
        dialogue.StartConversation(hints);
    }
    /////////////////
    //             //
    //  NO SHAME,  //
    // ONLY HINTS. //
    //             //
    /////////////////
}
