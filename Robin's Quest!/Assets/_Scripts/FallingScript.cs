﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FallingScript : MonoBehaviour
{
    public GameObject fallEffect;
    public bool water;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetType() == typeof(BoxCollider2D))
        {
            if (collision.tag == "Player")
            {
                if(collision.GetComponent<Animator>().GetBool("Falling") == false)
                {
                    StartCoroutine(Whoops("Player"));
                }
            }
            else if (collision.tag == "Enemies")
            {
                collision.transform.position = this.transform.position;
                StartCoroutine(Whoops("Enemies"));
            }
            else
            {

            }
        }
        
        IEnumerator Whoops(string w)
        {
            Vector3 startScale = collision.transform.localScale;

            if(w == "Player")
            {
                PlayerMovement.frozen = true;
                collision.GetComponent<PlayerScript>().iFrames = true;
                collision.GetComponent<Animator>().SetBool("Falling", true);
                if (water)
                {
                    Instantiate(fallEffect, transform.position, transform.rotation);
                }
                for (float i = 1; i > 0; i -= 0.2f)
                {
                    collision.transform.position = 
                         Vector3.MoveTowards(collision.transform.position,
                        this.transform.position, 0.33f);
                    collision.GetComponent<SpriteRenderer>().color =
                        new Color(i, i, i, i);
                    collision.transform.localScale = (startScale * i);
                    yield return new WaitForSeconds(0.075f);
                }
                if (!water)
                {
                    Instantiate(fallEffect, transform.position, transform.rotation);
                }
                collision.GetComponent<PlayerScript>().iFrames = false;
                collision.transform.localScale = startScale;
                collision.GetComponent<PlayerScript>().TakeDamage(1.0f, PlayerScript.DamageTypes.None);
                collision.transform.position = PlayerScript.lakitu;
                collision.GetComponent<Animator>().SetBool("Falling", false);
                yield return new WaitForSeconds(0.25f);
                collision.GetComponent<SpriteRenderer>().color =
                        new Color(1, 1, 1, 1);
                PlayerMovement.frozen = false;
                collision.GetComponent<Rigidbody2D>().constraints =
                    RigidbodyConstraints2D.FreezeRotation;
                yield return null;
            }
            else
            {
                if (collision.GetComponent<Enemies>().GetEnemyStatus() != Enemies.EnemyStatus.Falling)
                {
                    collision.GetComponent<Enemies>().SetEnemyStatus(Enemies.EnemyStatus.Falling);
                    if (water)
                    {
                        Instantiate(fallEffect, transform.position, transform.rotation);
                    }
                    for (float i = 1; i > 0; i -= 0.2f)
                    {
                        collision.transform.position = 
                            Vector3.MoveTowards(collision.transform.position,
                            this.transform.position, 0.33f);
                        collision.GetComponent<SpriteRenderer>().color =
                            new Color(i, i, i, i);
                        collision.transform.localScale = (startScale * i);
                        yield return new WaitForSeconds(0.075f);
                    }
                    if (!water)
                    {
                        Instantiate(fallEffect, transform.position, transform.rotation);
                    }
                    collision.GetComponent<Enemies>().TakeDamage(10);
                    yield return null;
                }
                
            }
        }
    }
}
