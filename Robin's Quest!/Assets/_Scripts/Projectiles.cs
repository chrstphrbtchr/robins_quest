﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Panda;

public class Projectiles : MonoBehaviour
{
    Vector2 projectileDirection;
    float projectileSpeed;

    public enum TypesOfProjectiles { Fire, Ice, Poison, Chaos, AOE, Homing, Sharp };
    public TypesOfProjectiles projectileType;

    Animator anim;

    /// <summary>
    /// Sets the direciton of the projectile.
    /// </summary>
    /// <param name="dir">The starting direction of the projectile.</param>
    public void SetDirection(Vector2 dir) => this.projectileDirection = dir;
    /// <summary>
    /// Returns the current direction of the projectile.
    /// </summary>
    /// <returns>The direction of the projectile as a Vector2.</returns>
    public Vector2 GetDirection() => this.projectileDirection;
    public TypesOfProjectiles GetProjectileType() => this.projectileType;

    private void Start()
    {
        anim = GetComponent<Animator>();
        // FMOD sound fx.
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Walls")
        {
            Explode();
        }
        else if(collision.gameObject.tag == "Player")
        {
            // player takes dmg.
            // FMOD sound fx
            // 
            Explode();
        }
    }

    void Explode()
    {
        anim.SetBool("hitSomething", true);
        Destroy(this.gameObject);
    }

    //=========================================================================================================
    /*
    public Vector2 startLoc;
    public Vector2 destiny;
    float turn;
    public GameObject player;
    public PlayerScript ps;
    public Animator anim;
    public string dir;
    public string elem;
    public float speed;
    public float dmg;
    bool hasBeenCast;
    bool hitSomething;

    void Start()
    {
        dmg = 1.25f; // <---------------------- this should go in the prefabs?
        speed = 3f;
        anim = GetComponent<Animator>();
        ps = FindObjectOfType<PlayerScript>();
        player = ps.gameObject;
        turn = 0f;
    }

    void Update()
    {
        if (!hitSomething)
        {
            if (Vector2.Distance(transform.position, destiny) > 0.1f)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                    destiny, speed * Time.deltaTime);
            }
            else
            {
                Explode();
            }
        }
    }

    public void CastSpell(string t, string d)
    {
        startLoc = new Vector2(transform.position.x, transform.position.y);

        elem = t;
        dir = d;

        switch (d)
        {
            // there might be more?
            case "N":
                destiny = startLoc + new Vector2(0, 10);
                turn = 90f;
                break;
            case "S":
                destiny = startLoc + new Vector2(0, -10);
                turn = -90f;
                break;
            case "E":
                destiny = startLoc + new Vector2(16,0);
                break;
            case "W": 
                destiny = startLoc + new Vector2(-16, 0); 
                turn = 180f;
                break;
            case "P":
                destiny = player.transform.position; 
                break;
            case "AOE":
                break;
            default:
                break;
        }

        transform.Rotate(0f, 0f, turn, Space.World);
    }

    void Explode()
    {
        hitSomething = true;
        anim.SetBool("hitSomething", true);
        Destroy(this.gameObject, 0.5f);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Walls")
        {
            Explode();
        }
        if(collision == ps.GetComponent<CapsuleCollider2D>())
        {
            Vector3 hitSpace = (ps.transform.position - transform.position);
            ps.GetComponent<Rigidbody2D>().AddForce(hitSpace * 5000, ForceMode2D.Impulse);
            ps.TakeDamage(dmg);
            Explode();
        }
    }
    */
}
