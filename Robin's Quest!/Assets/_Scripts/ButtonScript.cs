﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    public BoxCollider2D player;
    public Sprite buttonDown;
    public Sprite buttonUp;
    public bool permaPress;
    bool pressed;

    SpriteRenderer sr;

    public GameObject[] interactions;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        player = FindObjectOfType<PlayerScript>().GetComponent<BoxCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision == player) ||
            (collision.tag == "Movable"))
        {
            if (!pressed)
            {
                sr.sprite = buttonDown;
                foreach (GameObject a in interactions)
                {
                    if (permaPress)
                    {
                        Destroy(a);
                    }
                    else
                    {
                        a.SetActive(false);
                    }
                }
                pressed = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision == player) ||
            (collision.tag == "Movable"))
        {
            if((pressed) && (!permaPress))
            {
                sr.sprite = buttonUp;
                foreach (GameObject a in interactions)
                {
                    a.SetActive(true);
                }
                pressed = false;
            }
        }
    }

}
