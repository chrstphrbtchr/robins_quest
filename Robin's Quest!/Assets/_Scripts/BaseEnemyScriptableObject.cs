using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Enemy Statblock")]
public class BaseEnemyScriptableObject : ScriptableObject
{
    public int hp;
    public float dmg;
    public float speed;
    public float waitTime;
    public float deathTime;
    public float aggroDist;

    enum EnemyType
    {
        Blob, IceBlob, FireBlob,
        Wizard, IceWizard, FireWizard
    }; // Finish filling this out.
}
