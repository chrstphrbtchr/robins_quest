﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PauseScreen : MonoBehaviour
{
    // player
    //public PlayerScript PlayerScript;
    public PlayerMovement pm;

    // camera
    public Camera cam;

    // items
    public GameObject boots;
    public GameObject floppy;
    public GameObject gem;
    public GameObject glove;
    public GameObject hammer;
    public GameObject mush;
    public GameObject potion;
    public GameObject ring;
    public GameObject scroll;
    public GameObject shell;
    public GameObject shield;
    public GameObject shovel;
    public GameObject sword;

    // b&w items
    public GameObject bootsBW;
    public GameObject floppyBW;
    public GameObject gemBW;
    public GameObject gloveBW;
    public GameObject hammerBW;
    public GameObject mushBW;
    public GameObject potionBW;
    public GameObject ringBW;
    public GameObject scrollBW;
    public GameObject shellBW;
    public GameObject shieldBW;
    public GameObject shovelBW;

    // ui
    int cursorIndex;
    public string itemSelected;
    public GameObject itemScreen;
    public GameObject exitScreen;
    public GameObject cursor;

    public Dictionary<Vector2, string>
        cursorOverItems = new Dictionary<Vector2, string>()
        {
            {new Vector2(-320f,  90f), "HAMMER"   },
            {new Vector2(-185f,  90f), "SHOVEL"   },
            {new Vector2(-50f,   90f), "MUSHROOM" },
            {new Vector2(-320f, -50f), "GLOVE"    },
            {new Vector2(-185f, -50f), "GEM"      },
            {new Vector2(-50f,  -50f), "BOOTS"    },
            {new Vector2(-320f,-190f), "SCROLL"   },
            {new Vector2(-185f,-190f), "RING"     },
            {new Vector2(-50f, -190f), "FLOPPY"   }
        };
    public List<Vector2> cursorLocations =
        new List<Vector2>()
        {
            new Vector2(-320f,  90f),
            new Vector2(-185f,  90f),
            new Vector2(-50f,   90f),
            new Vector2(-320f, -50f), 
            new Vector2(-185f, -50f),
            new Vector2(-50f,  -50f),
            new Vector2(-320f,-190f),
            new Vector2(-185f,-190f),
            new Vector2(-50f, -190f)
        };

    List<GameObject> inactiveItems =
        new List<GameObject>();

    // bools
    public static bool paused;
    bool menuUp;
    bool escaped;

    // Start is called before the first frame update
    void Start()
    {
        cursorIndex = 0;
        inactiveItems = new List<GameObject>()
        {hammer, shovel, mush, boots, gem, 
            scroll, ring, glove, floppy};
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
        {
            if (Vector2.Distance(itemScreen.transform.localPosition,
            new Vector2(0, -200f)) > 0)
            {
                itemScreen.transform.localPosition =
                    Vector2.MoveTowards(itemScreen.transform.localPosition,
                    new Vector2(0, -200f), 0.5f);
            }
            // put all input for pause screen
            // here. Update will still run when
            // time is set to zero.

            // ----- cursor movement -----
            if ((Input.GetKeyDown(KeyCode.DownArrow)) ||
                (Input.GetKeyDown(KeyCode.S)))
            {
                if(cursorIndex + 3 <= (cursorLocations.Count - 1))
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex + 3];
                    cursorIndex += 3;
                }
                else
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex - 6];
                    cursorIndex -= 6;
                }
            }
            if ((Input.GetKeyDown(KeyCode.UpArrow)) ||
                (Input.GetKeyDown(KeyCode.W)))
            {
                if (cursorIndex - 3 >= 0)
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex - 3];
                    cursorIndex -= 3;
                }
                else
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex + 6];
                    cursorIndex += 6;
                }
            }
            if ((Input.GetKeyDown(KeyCode.LeftArrow)) ||
                (Input.GetKeyDown(KeyCode.A)))
            {
                if ((cursorIndex == 0) || (cursorIndex == 3) || (cursorIndex == 6))
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex + 2];
                    cursorIndex += 2;
                }
                else
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex - 1];
                    cursorIndex -= 1;
                }
            }
            if ((Input.GetKeyDown(KeyCode.RightArrow)) ||
                (Input.GetKeyDown(KeyCode.D)))
            {
                if ((cursorIndex == 2) || (cursorIndex == 5) || (cursorIndex == 8))
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex - 2];
                    cursorIndex -= 2;
                }
                else
                {
                    cursor.transform.localPosition = cursorLocations[cursorIndex + 1];
                    cursorIndex += 1;
                }
            }
            // ----------------------------

            if ((Input.GetKeyDown(KeyCode.RightShift)) ||
                (Input.GetKeyDown(KeyCode.LeftShift)) ||
                (Input.GetKeyDown(KeyCode.Space)))
            {
                Equip(cursorOverItems[cursorLocations[cursorIndex]]);
            }
        }
        else
        {
            if (Vector2.Distance(itemScreen.transform.localPosition,
            new Vector2(0, -210f)) > 0)
            {
                itemScreen.transform.localPosition =
                    Vector2.MoveTowards(itemScreen.transform.localPosition,
                    new Vector2(0, -210f), 0.5f);
            }
        }



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escaped = !escaped;
            EscapeScreen();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!paused)
            {
                Paused();
            }
            else
            {
                Unpaused();
            }
        }
    }

    void Paused()
    {
        paused = true;
        menuUp = true;
        Time.timeScale = 0;
    }

    void Unpaused()
    {
        paused = false;
        menuUp = false;
        Time.timeScale = 1;
    }

    public void Equip(string item)
    {
        foreach(GameObject g in inactiveItems)
        {
            g.SetActive(false);
        }

        switch (item)
        {
            case "HAMMER":
                if (PlayerScript.hammer)
                {
                    hammer.SetActive(true);
                    itemSelected = "HAMMER";
                    PlayerScript.equipped = PlayerScript.Items.Hammer;
                }
                break;
            case "SHOVEL":
                if (PlayerScript.shovel)
                {
                    shovel.SetActive(true);
                    itemSelected = "SHOVEL";
                    PlayerScript.equipped = PlayerScript.Items.Shovel;
                }
                break;
            case "MUSHROOM":
                if (PlayerScript.mush)
                {
                    mush.SetActive(true);
                    itemSelected = "MUSHROOM";
                    PlayerScript.equipped = PlayerScript.Items.Mushroom;
                }
                break;
            case "BOOTS":
                if (PlayerScript.boots)
                {
                    boots.SetActive(true);
                    itemSelected = "BOOTS";
                    PlayerScript.equipped = PlayerScript.Items.Boots;
                }
                break;
            case "GEM":
                if (PlayerScript.gem)
                {
                    gem.SetActive(true);
                    itemSelected = "GEM";
                    PlayerScript.equipped = PlayerScript.Items.Gem;
                }
                break;
            case "SCROLL":
                if (PlayerScript.scroll)
                {
                    scroll.SetActive(true);
                    itemSelected = "SCROLL";
                    PlayerScript.equipped = PlayerScript.Items.Scroll;
                }
                break;
            case "RING":
                if (PlayerScript.ring)
                {
                    ring.SetActive(true);
                    itemSelected = "RING";
                    PlayerScript.equipped = PlayerScript.Items.Ring;
                }
                break;
            case "GLOVE":
                if (PlayerScript.glove)
                {
                    glove.SetActive(true);
                    itemSelected = "GLOVE";
                    PlayerScript.equipped = PlayerScript.Items.Glove;
                }
                break;
            case "FLOPPY":
                if (PlayerScript.floppy)
                {
                    floppy.SetActive(true);
                    itemSelected = "FLOPPY";
                    PlayerScript.equipped = PlayerScript.Items.Floppy;
                }
                break;
            default:
                itemSelected = "NONE";
                PlayerScript.equipped = PlayerScript.Items.None;
                break;
        }
    }

    void EscapeScreen()
    {
        if (escaped)
        {
            paused = true;
        }
        else
        {
            if (!menuUp)
            {
                paused = false;
            }
        }
    }
}
