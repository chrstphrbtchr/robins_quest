﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WarpBoxes : MonoBehaviour
{
    public BoxCollider2D player;
    public Camera cambot;
    public CanvasGroup fader;
    public static bool warping;
    Dialogue d;

    public bool pNorth;
    public bool pSouth;
    public bool pEast;
    public bool pWest;

    public Vector2 camPos;
    public Vector2 playerPos;

    void Start()
    {
        d = FindObjectOfType<Dialogue>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == player)
        {
            //EnemyScript.baddies.Clear();
            StartCoroutine(Keymaster());
            if (player.GetComponent<PlayerScript>().attacking)
            {
                player.GetComponent<PlayerScript>().attacking = false;
                Destroy(FindObjectOfType<SwordScript>().gameObject);
            }
            //foreach (GameObject e in EnemyScript.baddies) FIX!!!!!!!!!!!!!!!!!!!
            //{
            //    Destroy(e);
            //}
        }
    }

    IEnumerator Keymaster()
    {
        warping = true;
        PlayerMovement.frozen = true;
        player.GetComponent<PlayerScript>().iFrames = true;
        for (int i = 0; i < 20; i++)
        {
            cambot.GetComponent<Pixelate>().pixelSizeX += 1;
            fader.alpha += 0.05f;
            yield return new WaitForSeconds(.025f);
        }

        Gatekeeper();
        yield return new WaitForSeconds(.05f);

        for (int i = 0; i < 20; i++)
        {
            cambot.GetComponent<Pixelate>().pixelSizeY -= 1;
            fader.alpha -= 0.05f;
            yield return new WaitForSeconds(.025f);
        }
        PlayerMovement.frozen = false;
        player.GetComponent<Rigidbody2D>().constraints =
            RigidbodyConstraints2D.FreezeRotation;
        player.GetComponent<PlayerScript>().iFrames = false;
        warping = false;
        yield return null;
    }

    private void Gatekeeper()
    {
        cambot.transform.position = new Vector3(camPos.x, camPos.y, -10f);
        player.transform.position = playerPos;
        d.UpdateCamera(cambot.transform.position);
        player.GetComponent<Animator>().SetBool("North", pNorth);
        player.GetComponent<Animator>().SetBool("South", pSouth);
        player.GetComponent<Animator>().SetBool("East", pEast);
        player.GetComponent<Animator>().SetBool("West", pWest);
    }
}
