﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    UnityEngine.Vector2 movement;

    public Rigidbody2D rb;
    public Animator animator;
    public SpriteRenderer sprite_r;

    // movement speed
    public float move_speed = 5f;

    // position / direction freezing
    static public bool frozen;
    static public bool jumping;

    void Update()
    {
        animator.SetBool("Frozen", frozen);

        if ((!frozen) && (!PauseScreen.paused))
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
            movement = movement.normalized;

            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);
            animator.SetFloat("Speed", movement.sqrMagnitude);

            if ((movement.x == 0) && (movement.y == 0))
            {
                if ((Input.GetKeyUp(KeyCode.W)) || (Input.GetKeyUp(KeyCode.UpArrow)))
                {
                    animator.SetBool("North", true);
                    animator.SetBool("South", false);
                    animator.SetBool("East", false);
                    animator.SetBool("West", false);
                }
                if ((Input.GetKeyUp(KeyCode.A)) || (Input.GetKeyUp(KeyCode.LeftArrow)))
                {
                    animator.SetBool("North", false);
                    animator.SetBool("South", false);
                    animator.SetBool("East", false);
                    animator.SetBool("West", true);
                }
                if ((Input.GetKeyUp(KeyCode.S)) || (Input.GetKeyUp(KeyCode.DownArrow)))
                {
                    animator.SetBool("North", false);
                    animator.SetBool("South", true);
                    animator.SetBool("East", false);
                    animator.SetBool("West", false);
                }
                if ((Input.GetKeyUp(KeyCode.D)) || (Input.GetKeyUp(KeyCode.RightArrow)))
                {
                    animator.SetBool("North", false);
                    animator.SetBool("South", false);
                    animator.SetBool("East", true);
                    animator.SetBool("West", false);
                }
            }
            else
            {
                animator.SetBool("North", false);
                animator.SetBool("South", false);
                animator.SetBool("East", false);
                animator.SetBool("West", false);
            }
        }
        else
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    void FixedUpdate()
    {
        if (!frozen)
        {
            // mvmt
            rb.MovePosition(rb.position + movement * move_speed * Time.fixedDeltaTime);
        }
    }

    public IEnumerator Jumpman()
    {
        jumping = true;
        GetComponent<BoxCollider2D>().enabled = false;
        animator.SetBool("Jumping", true);
        move_speed = move_speed * 1.5f;
        yield return new WaitForSeconds(0.25f);
        move_speed = move_speed / 1.5f;
        animator.SetBool("Jumping", false);
        GetComponent<BoxCollider2D>().enabled = true;
        jumping = false;
        yield return null;
    }
}
