﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCs : MonoBehaviour
{
    public string charName;
    public Dialogue dialogue;
    public int topic;

    Animator anim;
    Rigidbody2D rb;

    RaycastHit2D[] hits;
    public bool stationary;

    public int face;
    float speed = 1.25f;
    public bool isWalking;
    public float walkTime;
    public float idleTime;
    float walkCount;
    float idleCount;

    public List<List<string>> chitchat = new List<List<string>>();

    void Start()
    {
        ConversationStarter(charName);
        idleCount = idleTime;
        walkCount = walkTime;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        dialogue = FindObjectOfType<Dialogue>();
        if (!stationary)
        {
            GoDirection();
        }
    }

    void Update()
    {
        anim.SetBool("isWalking", isWalking);
        anim.SetFloat("Face", (0.2f * face));

        if (!PlayerMovement.frozen)
        {
            if (isWalking)
            {
                walkCount -= Time.deltaTime;
                switch (face)
                {
                    case 0:
                        rb.velocity = new Vector2(0, speed);
                        hits = Physics2D.RaycastAll(transform.position, Vector2.up, .5f);
                        //Debug.DrawRay(transform.position, Vector2.up, Color.black, .5f);
                        break;
                    case 1:
                        rb.velocity = new Vector2(0, -speed);
                        hits = Physics2D.RaycastAll(transform.position, Vector2.down, .5f);
                        //Debug.DrawRay(transform.position, Vector2.down, Color.black, .5f);
                        break;
                    case 2:
                        rb.velocity = new Vector2(speed, 0);
                        hits = Physics2D.RaycastAll(transform.position, Vector2.right, .5f);
                        //Debug.DrawRay(transform.position, Vector2.right, Color.black, .5f);
                        break;
                    case 3:
                        rb.velocity = new Vector2(-speed, 0);
                        hits = Physics2D.RaycastAll(transform.position, Vector2.left, .5f);
                        //Debug.DrawRay(transform.position, Vector2.left, Color.black, .5f);
                        break;
                    case 4:
                        //Debug.LogError("FOUR");
                        break;
                    default:
                        break;
                        
                }
                foreach(RaycastHit2D r in hits)
                {
                    if( (r.collider.tag != "NPCs"  ) &&
                        (r.collider.tag != "Player") &&
                        (r.collider.tag != "Untagged"))
                    {
                        // basically, if it is an obstacle
                        // of any kind (rock, tree, wall,
                        // water, pit, enemy, etc).
                        face = Random.Range(0, 4);
                    }
                }
                if (walkCount < 0)
                {
                    idleCount = idleTime;
                    isWalking = false;
                }
            }
            else
            {
                idleCount -= Time.deltaTime;
                rb.velocity = Vector2.zero;

                if (idleCount < 0)
                { 
                    GoDirection();
                }
            }
        }
        else
        {
            idleCount = idleTime;
            isWalking = false;
        }
    }

    public void HelloPrincess(int d)
    {
        PlayerScript.heyListen = true;
        face = d;
        dialogue.StartConversation(chitchat);
    }

    public void ConversationStarter(string n)
    {
        switch (n)
        {
            case "BEN":
                List<string> ben = new List<string>()
                {
                    "Me? Oh, I don't swim.",
                    "It's too dangerous!"
                };
                chitchat.Add(ben);
                break;
            case "SHOPKEEPER":
                break;
            case "CHRISTOPHER":
                List<string> chris = new List<string>()
                {
                    "<s>HELP!</s>",
                    "I'm stuck in my own",
                    "game!"
                };
                chitchat.Add(chris);
                break;
            default:
                break;
        }
    }
    public void GoDirection()
    {
        face = Random.Range(0, 4);
        isWalking = true;
        walkCount = walkTime;
    }

    public void OffScreen()
    {
        switch (face)
        {
            case 1:
                face = 0;
                break;
            case 0:
                face = 1;
                break;
            case 3:
                face = 2;
                break;
            case 2:
                face = 3;
                break;
            case 4:
                //Debug.LogError("Four");
                break;
            default:
                break;
        }
        isWalking = true;
        walkCount = walkTime;
    }
}
