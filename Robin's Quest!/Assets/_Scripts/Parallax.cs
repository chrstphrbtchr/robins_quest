﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    float imgLength;
    float scrollPos;

    public bool scrollable;
    public bool scrollRight;
    public bool goingUp;

    public float parallaxEffect;

    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        goingUp = true; //
        scrollPos = transform.position.x;
        imgLength = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = (cam.transform.position.y * parallaxEffect);

        transform.position =
                new Vector3(transform.position.x, dist, transform.position.z);

        if (scrollable)
        {
            if (scrollRight)
            {
                transform.position = new Vector3(transform.position.x + (0.25f * Time.deltaTime),
                    transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x - (0.25f * Time.deltaTime),
                    transform.position.y, transform.position.z);
            }

            if(transform.localPosition.x >= 35f)
            {
                transform.localPosition = new Vector3(transform.localPosition.x - 54f, 
                    transform.localPosition.y, transform.localPosition.z);
            }
            else if(transform.localPosition.x <= -35f)
            {
                transform.localPosition = new Vector3(transform.localPosition.x + 54f,
                                    transform.localPosition.y, transform.localPosition.z);
            }
        }
    }
}
