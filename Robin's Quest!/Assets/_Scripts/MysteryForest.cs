﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MysteryForest : MonoBehaviour
{
    string doorName;
    public char dir;
    static string pathway;
    static bool moving;
    
    void Awake()
    {
        doorName = this.name;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.tag == "Player") && (!moving))
        {
            if (PlayerScript.ring && PlayerScript.scroll)
            {
                pathway = pathway + dir;
            }
            moving = true;
            switch (doorName)
            {
                case "MYSTERY WARP N":
                    collision.transform.position =
                        new Vector3(-90.5f, -16.25f, 0f);
                    Debug.Log(collision.transform.position);
                    break;
                case "MYSTERY WARP S":
                    collision.transform.position =
                        new Vector3(-90.5f, -5.75f, 0f);
                    break;
                case "MYSTERY WARP E":
                    // warp out.
                    collision.transform.position =
                        new Vector3(-104.75f, -15f, 0f);
                    pathway = "";
                    break;
                case "MYSTERY WARP W":
                    collision.transform.position =
                        new Vector3(-81.75f, -11.5f, 0f);
                    break;
                default:
                    Debug.LogError("No such pathway");
                    break;
            }


            if(pathway == "NSEW") // <- solution.
            {
                LinkToTheFuture();
            }
            else
            {
                pathway = "";
            }
        } 
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            moving = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            moving = false;
        }
    }

    void LinkToTheFuture()
    {
        // play success sound
        // teleport to Warp Box
        FindObjectOfType<PlayerScript>().transform.position = 
            new Vector3(-104.75f, -6f, 0f);
        // teleport to glitch forest.
    }
}
