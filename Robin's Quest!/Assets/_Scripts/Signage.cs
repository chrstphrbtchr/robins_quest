﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signage : MonoBehaviour
{
    [TextArea]
    public List<string> words;
    List<List<string>> post;
    Dialogue dial;

    private void Awake()
    {
        dial = FindObjectOfType<Dialogue>();
    }

    public void Announcement()
    {
        post.Add(words);
        dial.StartConversation(post);
        post.Clear();
    }
}
