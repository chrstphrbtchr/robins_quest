using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Panda;

public class Wizard : Enemies
{
    GameObject projectilePrefab;

    Vector2 SetNextMove()
    {
        Vector2 result = Vector2.zero;

        switch (GetDirection())
        {
            case Directions.North:
                this.nextMove = Vector2.up;
                break;
            case Directions.South:
                this.nextMove = Vector2.up * -1;
                break;
            case Directions.East:
                this.nextMove = Vector2.right;
                break;
            case Directions.West:
                this.nextMove = Vector2.right * -1;
                break;
            default:
                Debug.LogError("Next move cannot be determined.");
                break;
        }

        return result;
    }

    [Task]
    void ShootProjectile()
    {
        GameObject projectile = Instantiate(projectilePrefab, this.transform);
        projectile.GetComponent<Projectiles>().SetDirection(nextMove);
    }
}
