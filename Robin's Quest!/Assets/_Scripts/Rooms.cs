﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rooms : MonoBehaviour
{
    public CircleCollider2D player;
    public string room_name;
    public Camera cambot;
    public Dialogue d;

    public bool hasKey;
    public bool hasBossKey;
    static string playerRoom;
    static bool moving;
    public Vector3 safetyNet; // set for if items are out of reach. 
    List<GameObject> enemyList = new List<GameObject>();
    static List<Transform> linkBetweenRooms = new List<Transform>() { };

    // enemy prefabs:
    public GameObject wiz_prefab;
    public GameObject blob_prefab;

    // npc prefabs:
    public GameObject dog;
    public GameObject blu;
    public GameObject gre;
    public GameObject lav;
    public GameObject lim;
    public GameObject pur;
    public GameObject red;
    public GameObject sky;
    public GameObject vio;
    public GameObject yel;

    void Start()
    {
        room_name = this.name;
        d = FindObjectOfType<Dialogue>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision == player)
        {
            //EnemyScript.baddies.Clear();
            playerRoom = room_name;
            linkBetweenRooms.Add(transform);
            StartCoroutine(CameraShift());

            switch (room_name)
            {
                case "Town 1":
                    break;
                case "Town 2":
                    MonsterFactory("BLOB", new Vector3(16, -10, 0));
                    break;
                case "Town 3":
                    MonsterFactory("WIZARD", new Vector3(21, -1, 0));
                    MonsterFactory("WIZARD", new Vector3(19, 0, 0));
                    break;
                case "Town 6":
                    // Dungeon 2 entrance.
                    break;
                case "Town 7":
                    PlayerScript.lakitu = new Vector3(-16.5f, 0, 0);
                    break;
                case "Town 16":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Town 18":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Town 19":
                    PlayerScript.lakitu = new Vector3(60, -9.5f, 0);
                    break;
                case "Dungeon A01":
                    PlayerScript.lakitu = new Vector3(-160f, -3f, 0);
                    PlayerScript.respawn = new Vector3(-160f, -3.75f, 0);
                    break;
                case "Dungeon A02":
                    MonsterFactory("BLOB", new Vector3(-160f, 10f, 0));
                    break;
                case "Dungeon A03":
                    PlayerScript.lakitu = new Vector3(-176f, 10f, 0);
                    break;
                case "Dungeon A06":
                    MonsterFactory("BLOB", new Vector3(-148.5f, 1, 0));
                    MonsterFactory("BLOB", new Vector3(-148.5f, -2f, 0));
                    MonsterFactory("BLOB", new Vector3(-139.5f, 1, 0));
                    MonsterFactory("BLOB", new Vector3(-139.5f, -2f, 0));
                    break;
                case "Dungeon A07":
                    // bats!
                    break;
                case "Dungeon A08":
                    PlayerScript.lakitu = new Vector3(-167f, -10.5f, 0);
                    MonsterFactory("WIZARD", new Vector3(-157f, -10.5f, 0f));
                    break;
                case "Dungeon A11":
                    MonsterFactory("BIGBLOB", new Vector3(-155.5f, 27, 0));
                    break;
                case "Beach 3":
                    PlayerScript.lakitu = new Vector3(27, -28f, 0);
                    break;
                case "Beach 4":
                    PlayerScript.lakitu = new Vector3(12, -31f, 0);
                    break;
                case "Beach 5":
                    PlayerScript.lakitu = new Vector3(0, -30f, 0);
                    break;
                case "Beach 6":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Beach 7":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Beach 9":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Beach 10":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Beach 11":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Beach 17":
                    PlayerScript.lakitu = new Vector3(24.5f, -35.5f, 0);
                    break;
                case "Beach 18":
                    PlayerScript.lakitu = new Vector3(22, -36f, 0);
                    break;
                case "City 5":
                    PlayerScript.lakitu = new Vector3(-80, 50, 0);
                    break;
                case "City 10":
                    PlayerScript.lakitu = new Vector3(-80, 50, 0);
                    break;
                case "City 19":
                    PlayerScript.lakitu = new Vector3(-80, 50, 0);
                    break;
                case "Island Shack 1":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Island Shack 2":
                    PlayerScript.lakitu = new Vector3(0, -44f, 0);
                    break;
                case "Underground 1":
                    PlayerScript.lakitu = new Vector3(95.5f, 12f, 0);
                    break;
                default:
                    break;
            }
            d.UpdateCamera(this.transform.position);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == player)
        {
            // despawns all enemies in previous room
            foreach (GameObject e in enemyList)
            {
                Destroy(e);
            }
            linkBetweenRooms.Remove(transform);

            switch (room_name)
            {
                // This area used to turn off traps & such.
                case "Town 3":
                    break;
                default:
                    break;
            }
        }
        else if(collision.tag == "NPC")
        {
            collision.GetComponent<NPCs>().OffScreen();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if((collision == player) && !moving)
        {
            if((linkBetweenRooms.Count <= 1)
                && (playerRoom != room_name))
            {
                StartCoroutine(CameraShift());
                d.UpdateCamera(this.transform.position);
            }
        }
    }

    void MonsterFactory(string bad_name, Vector2 bad_place)
    {
        switch (bad_name)
        {
            case "WIZARD": //FIX ALL
                GameObject wiz = Instantiate(wiz_prefab, bad_place, this.transform.rotation);
                //wiz.GetComponent<EnemyScript>().StatBlock(bad_name);
                if (hasKey)
                {
                    //wiz.GetComponent<EnemyScript>().hasKey = true;
                }
                if (hasBossKey)
                {
                    //wiz.GetComponent<EnemyScript>().hasBossKey = true;
                }
                //wiz.GetComponent<EnemyScript>().hometown = this.gameObject;
                enemyList.Add(wiz);
                break;
            case "BLOB":
                GameObject blob = Instantiate(blob_prefab, bad_place, this.transform.rotation);
                //blob.GetComponent<EnemyScript>().StatBlock(bad_name);
                enemyList.Add(blob);
                break;
            case "BIGBLOB":
                GameObject bigblob = Instantiate(blob_prefab, bad_place, this.transform.rotation);
                /*
                bigblob.GetComponent<EnemyScript>().phase = 5;
                bigblob.GetComponent<EnemyScript>().StatBlock(bad_name);
                StartCoroutine(bigblob.GetComponent<EnemyScript>().BigBlob(5));
                enemyList.Add(bigblob);*/
                break;
            default:
                // No one's here!
                break;
        }
    }

    void NPCFactory(string n, Vector2 p)
    {
        switch (n)
        {
            case "CHRISTOPHER":
                Debug.Log("HELP I AM STUCK IN MY OWN GAME!");
                break;
            default:
                Debug.LogError(n + " does not exist @ " + p);
                break;
        }
    }
    
    public void Locksmith()
    {
        if(room_name == playerRoom)
        {
            if (hasBossKey)
            {
                hasBossKey = false;
            }
            if (hasKey)
            {
                hasKey = false;
            }
        }
    }

    IEnumerator CameraShift()
    {
        moving = true;
        if (!WarpBoxes.warping)
        {
            PlayerMovement.frozen = true;
            while (Vector3.Distance(cambot.transform.position,
                this.transform.position) > 0)
            {
                cambot.transform.position =
                    Vector3.MoveTowards(cambot.transform.position,
                    this.transform.position, 0.5f);
                yield return null;
            }
            PlayerMovement.frozen = false;
            player.GetComponentInParent<Rigidbody2D>().constraints =
                RigidbodyConstraints2D.FreezeRotation;
        }
        moving = false;
        yield return null;
    }
}
