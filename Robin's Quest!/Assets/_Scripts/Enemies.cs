using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Panda;

public class Enemies : MonoBehaviour
{
    public BaseEnemyScriptableObject statblock;

    //Base Stats:
    int hp;
    float dmg;
    float speed;
    float waitTime;
    float deathTime;
    float aggroDist;

    public enum EnemyStatus
    {
        Attacking, Damaged, Dying, Falling, Idle, Swimming, Walking
    };
    public enum Directions { North, South, East, West };
    Directions currentDir = Directions.South;

    EnemyStatus enemyState = EnemyStatus.Idle;

    Animator animator;

    public Vector2 nextMove;
    Vector2 lookDirection;

    void Awake()
    {
        if(statblock != null)
        {
            SetHP(              statblock.hp        );
            SetDMG(             statblock.dmg       );
            SetSpeed(           statblock.speed     );
            SetWaitTime(        statblock.waitTime  );
            SetDeathTime(       statblock.deathTime );
            SetAggroDistance(   statblock.aggroDist );
        }
        else
        {
            Destroy(this.gameObject);
            Debug.LogError("No statblock provided for " + this.gameObject.name);
        }
    }

    void FixedUpdate()
    {
        DetermineDirection();
    }

    // Getters & Setters
    public int GetHP() => this.hp;
    public void SetHP(int health) => this.hp = health;
    public float GetDMG() => this.dmg;
    public void SetDMG(float damage) => this.dmg = damage;
    public float GetSpeed() => this.speed;
    public void SetSpeed(float s) => this.speed = s;
    public float GetWaitTime() => this.waitTime;
    public void SetWaitTime(float w) => this.waitTime = w;
    public float GetDeathTime() => this.deathTime;
    public void SetDeathTime(float d) => this.deathTime = d;
    public float GetAggroDistance() => this.aggroDist;
    public void SetAggroDistance(float dist) => this.aggroDist = dist;
    public EnemyStatus GetEnemyStatus() => this.enemyState;
    public void SetEnemyStatus(EnemyStatus e) => this.enemyState = e;
    public Directions GetDirection() => this.currentDir;
    public void SetDirection(Directions dir) => this.currentDir = dir;
    public bool IsAlive() => GetHP() > 0;


    void DetermineDirection()
    {
        Vector2 playerPosition = GetPlayerPosition();

        if (playerPosition.y > transform.position.y)
        {
            if ((playerPosition.x > transform.position.x) && 
                ((playerPosition.x - transform.position.x) >
                (playerPosition.y - transform.position.y)))
            {
                SetDirection(Directions.East);
            }
            else if ((playerPosition.x < transform.position.x) && 
                ((transform.position.x - playerPosition.x) >
                (playerPosition.y - transform.position.y)))
            {
                SetDirection(Directions.West);
            }
            else
            {
                SetDirection(Directions.North);
            }
        }
        else if (playerPosition.y < transform.position.y)
        {
            if ((playerPosition.x > transform.position.x) && 
                ((playerPosition.x - transform.position.x) >
                (transform.position.y - playerPosition.y)))
            {
                SetDirection(Directions.East);
            }
            else if ((playerPosition.x < transform.position.x) && 
                ((transform.position.x - playerPosition.x) >
                (transform.position.y - playerPosition.y)))
            {
                SetDirection(Directions.West);
            }
            else
            {
                SetDirection(Directions.South);
            }
        }
        else
        {
            if (playerPosition.x > transform.position.x)
            {
                SetDirection(Directions.East);
            }
            else if (playerPosition.x < transform.position.x)
            {
                SetDirection(Directions.West);
            }
            else
            {
                SetDirection(Directions.South);
            }
        }

        switch (GetDirection())
        {
            case Directions.North:
                //nextMove =      Vector2.up;
                lookDirection = transform.up;
                animator.SetBool("North",   true    );
                animator.SetBool("South",   false   );
                animator.SetBool("East",    false   );
                animator.SetBool("West",    false   );
                break;
            case Directions.South:
                //nextMove =      Vector2.down;
                lookDirection = (-1 * transform.up);
                animator.SetBool("North",   false   );
                animator.SetBool("South",   true    );
                animator.SetBool("East",    false   );
                animator.SetBool("West",    false   );
                break;
            case Directions.East:
                //nextMove =      Vector2.right;
                lookDirection = transform.right;
                animator.SetBool("North",   false   );
                animator.SetBool("South",   false   );
                animator.SetBool("East",    true    );
                animator.SetBool("West",    false   );
                break;
            case Directions.West:
                //nextMove =      Vector2.left;
                lookDirection = (-1 * transform.right);
                animator.SetBool("North",   false   );
                animator.SetBool("South",   false   );
                animator.SetBool("East",    false   );
                animator.SetBool("West",    true    );
                break;
            default:
                Debug.LogError("No Direction Provided: " + this.name + this.transform.position + ".");
                break;
        }
    }

    Vector2 GetPlayerPosition() => GameObject.FindGameObjectWithTag("Player").transform.position;
    bool WithinRange() => (Vector2.Distance(this.transform.position, GetPlayerPosition()) < aggroDist);
    

    public void TakeDamage(int damage)
    {
        if(damage < hp)
        {
            hp -= damage;
            // dmg anim.
            // etc.
        }
        else
        {
            SetHP(0);
            Die();
        }
    }

    void Die()
    {
        // What happens when an enemy dies.
        Despawn();
    }

    public void Fall(bool splash)
    {
        if (splash)
        {
            // Fall in water.
        }
        else
        {
            // Fall off cliff.
        }

        Die();
    }

    public void Spawn()
    {
        // Turn on relevant components when in range.
        SetHP(statblock.hp);
    }

    public void Despawn()
    {
        // Turn off relevant components when not in range.
    }
}
