﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBlock : MonoBehaviour
{
    public BoxCollider2D player;
    public bool vert;
    public float landingZone;
    float timer;

    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>().GetComponent<BoxCollider2D>();
    }
    private void Update()
    {
        if ((!PlayerMovement.jumping) &&
            (timer > 0.5f))
        {
            StartCoroutine(JumpDown());
            timer = 0;
            
            PlayerMovement.jumping = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision == player)
        {
            if (vert == true)
            {
                if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0)
                {
                    timer += (1 * Time.deltaTime);
                }
                else
                {
                    timer = 0;
                }
            }
            else
            {
                if (Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0)
                {
                    timer += (1 * Time.deltaTime);
                }
                else
                {
                    timer = 0;
                }
            }
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == player)
        {
            timer = 0;
        }
    }

    IEnumerator JumpDown()
    {
        PlayerMovement.frozen = true;
        Vector3 takeoff = player.transform.position;
        player.GetComponent<BoxCollider2D>().enabled = false;
        player.GetComponent<Animator>().SetBool("Jumping", true);

        if (vert == true)
        {
            while(Vector3.Distance(player.transform.position,
                new Vector3(takeoff.x + landingZone,
                takeoff.y, takeoff.z)) > 0)
            {
                player.transform.position = 
                    Vector3.MoveTowards(player.transform.position,
                    new Vector3(takeoff.x + landingZone,
                    takeoff.y, takeoff.z), 0.25f);
                yield return new WaitForSeconds(0.025f);
            }
        }
        else
        {
            while (Vector3.Distance(player.transform.position,
                new Vector3(takeoff.x, takeoff.y + landingZone,
                takeoff.z)) > 0)
            {
                player.transform.position = 
                    Vector3.MoveTowards(player.transform.position,
                    new Vector3(takeoff.x, takeoff.y + landingZone, 
                    takeoff.z), 0.25f);
                yield return new WaitForSeconds(0.025f);
            }
        }
        player.GetComponent<Animator>().SetBool("Jumping", false); ;
        player.GetComponent<BoxCollider2D>().enabled = true;
        PlayerMovement.jumping = false;
        PlayerMovement.frozen = false;
        player.GetComponent<Rigidbody2D>().constraints =
            RigidbodyConstraints2D.FreezeRotation;
        yield return null;
    }
}
