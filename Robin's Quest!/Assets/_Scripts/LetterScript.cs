﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.WSA;

public class LetterScript : MonoBehaviour
{
    public bool wave;
    public bool shake;
    public float speed = 1f;
    Vector3 posL;
    Vector3 posS;

    private void Start()
    {
        posL = transform.position;
        posS = transform.position;
    }

    void Update()
    {
        if (wave)
        {
            StartCoroutine(WavyText(0.1f));
            wave = false;
        }

        if (shake)
        {
            StartCoroutine(ShakyText());
            shake = false;
        }
    }

    IEnumerator ShakyText()
    {
        transform.position = posL;
        posS.x += Random.Range(-0.05f, 0.05f);
        posS.y += Random.Range(-0.05f, 0.05f);
        transform.position = posS;
        posS = posL;
        yield return new WaitForSeconds(0.025f);
        StartCoroutine(ShakyText());
    }
    IEnumerator WavyText(float f)
    {
        posS.y = posL.y + f;
        while (Vector2.Distance(transform.position, posS) > 0)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                posS, 0.01f);
            yield return new WaitForSeconds(0.025f);
        }
        yield return new WaitForSeconds(0.01f);
        StartCoroutine(WavyText(f * (-1f)));
    }
}
