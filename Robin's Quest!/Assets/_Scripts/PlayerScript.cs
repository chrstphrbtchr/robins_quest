﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    // movement
    PlayerMovement pmvmt;

    // attacking
    public bool attacking;
    float atk_cooldown;
    float start_cooldown = 0.2f;

    // interacting
    static public bool heyListen;
    public bool interacting;
    RaycastHit2D[] lookRay;
    public int NPC_look;

    public enum Items { None, Sword, Hammer, Shovel, 
        Mushroom, Boots, Gem, Scroll, Ring, Glove, Floppy };
    public static Items equipped = Items.None;

    public enum DamageTypes { None, Fire, Slashing, Bludgeoning, Ice, Poison};

    // player stats
    public int maxHP;
    public float currentHP;
    int deaths;

    // death, etc.
    public static Vector3 respawn;
    public static Vector3 lakitu;
    public bool iFrames;

    // item bools, etc.
    static public bool sword;
    static public bool hammer;
    static public bool shovel;
    static public bool mush;
    static public bool boots;
    static public bool gem;
    static public bool scroll;
    static public bool ring;
    static public bool glove;
    static public bool floppy;
    static public bool bossKey;
    static public int keys;

    // respawn locations
    Vector2 home = new Vector2(0f, -12f);

    // sword
    public GameObject sword_prefab;

    // hp
    public HealthBar healthScript;

    void Start()
    {
        pmvmt = GetComponent<PlayerMovement>();
        currentHP = maxHP;
        respawn = new Vector3(0.5f, -11.25f, 0);
    }

    void Update()
    {
        if ((!PauseScreen.paused) && (!PlayerMovement.frozen))
        {
            // attack button
            if ((Input.GetKeyDown(KeyCode.Space)) && (!attacking))
            {
                Sworderang();
            }

            if (atk_cooldown > 0)
            {
                atk_cooldown -= Time.deltaTime;
            }
            else
            {
                GetComponent<Animator>().SetBool("Attacking", false);
            }

            // interact button
            if((Input.GetKeyDown(KeyCode.RightShift) ||
                (Input.GetKeyDown(KeyCode.LeftShift))))
            {
                InteractiveZone(); //

                if (!InteractiveZone())
                {
                    switch (equipped)
                    {
                        case Items.Boots:
                            if (!PlayerMovement.jumping)
                            {
                                StartCoroutine(pmvmt.Jumpman());
                            }
                            break;
                        case Items.Hammer:
                            atk_cooldown = start_cooldown; // use for all interactions.
                            GetComponent<Animator>().SetBool("Attacking", true);
                            break;
                        default:
                            break;
                    }
                }

                if (heyListen)
                {
                    Dialogue.currentTypeTime = 0;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Key")
        {
            keys += 1;
            Destroy(collision.gameObject);
        }
        else if(collision.tag == "Boss Key")
        {
            bossKey = true;
            Destroy(collision.gameObject);
        }

        if(collision.tag == "Key Door")
        {
            if(keys > 0)
            {
                Destroy(collision.gameObject);
                keys -= 1;
            }
        }
        else if(collision.tag == "Boss Door")
        {
            if (bossKey)
            {
                Destroy(collision.gameObject);
                bossKey = false;
            }
        }
    }


    void Sworderang()
    {
        if(sword == true)
        {
            // make the sword
            GameObject new_sword = Instantiate(sword_prefab);
            new_sword.GetComponent<SwordScript>().player = this.gameObject;

            // start attack
            atk_cooldown = start_cooldown;
            GetComponent<Animator>().SetBool("Attacking", true);
            attacking = true;

            // set sword destination
            if ((GetComponent<Animator>().GetFloat("Vertical") > 0f) ||
                (GetComponent<Animator>().GetBool("North") == true))
            {
                new_sword.GetComponent<SwordScript>().Attack("N");
            }
            else if ((GetComponent<Animator>().GetFloat("Vertical") < 0f) ||
                (GetComponent<Animator>().GetBool("South") == true))
            {
                new_sword.GetComponent<SwordScript>().Attack("S");
            }
            else if ((GetComponent<Animator>().GetFloat("Horizontal") > 0f) ||
                (GetComponent<Animator>().GetBool("East") == true))
            {
                new_sword.GetComponent<SwordScript>().Attack("E");
            }
            else
            {
                new_sword.GetComponent<SwordScript>().Attack("W");
            }
        }
        else
        {
            // This case is only for the
            // beginning when you do not
            // yet have your Sworderang.
            atk_cooldown = start_cooldown;
            GetComponent<Animator>().SetBool("Attacking", true);
        }
    }

    bool InteractiveZone()
    {
        if ((GetComponent<Animator>().GetFloat("Vertical") > 0f) ||
                (GetComponent<Animator>().GetBool("North") == true))
        {
            Vector3 offset = new Vector3(0, .5f, 0);
            lookRay = Physics2D.RaycastAll(transform.position + offset, Vector2.up, 0.5f);
            NPC_look = 1;
        }
        else if ((GetComponent<Animator>().GetFloat("Vertical") < 0f) ||
            (GetComponent<Animator>().GetBool("South") == true))
        {
            Vector3 offset = new Vector3(0, .5f, 0);
            lookRay = Physics2D.RaycastAll(transform.position - offset, Vector2.down, 0.5f);
            NPC_look = 0;
        }
        else if ((GetComponent<Animator>().GetFloat("Horizontal") > 0f) ||
            (GetComponent<Animator>().GetBool("East") == true))
        {
            Vector3 offset = new Vector3(.5f, 0, 0);
            lookRay = Physics2D.RaycastAll(transform.position + offset, Vector2.right, 0.5f);
            NPC_look = 3;
        }
        else
        {
            Vector3 offset = new Vector3(.5f, 0, 0);
            lookRay = Physics2D.RaycastAll(transform.position - offset, Vector2.left, 0.5f);
            NPC_look = 2;
        }

        foreach (RaycastHit2D r in lookRay)
        {
            if (r.collider.tag == "NPCs")
            {
                if (!heyListen)
                {
                    r.collider.GetComponent<NPCs>().HelloPrincess(NPC_look);
                }
                return true;
            }
            else if(r.collider.tag == "Shell")
            {
                if (!heyListen)
                {
                    r.collider.GetComponent<HintShell>().Hintmaster();
                }
                return true;
            }
            else if(r.collider.tag == "Sign")
            {
                if (!heyListen)
                {
                    r.collider.GetComponent<Signage>().Announcement();
                }
                return true;
            }
        }
        return false;
    }

    public void TakeDamage(float dmgAmount, DamageTypes dmgType)
    {
        if (!iFrames)
        {
            currentHP = currentHP - dmgAmount;

            if (currentHP > 0f)
            {
                healthScript.UpdateHealth(currentHP);
                GetComponent<Animator>().SetBool("Hurt", true);
                StartCoroutine(Invincibility()); 
            }
            else
            {
                deaths++;
                // death anim. + etc.
            }
        }
    }

    IEnumerator Invincibility()
    {
        iFrames = true;
        if (GetComponent<Animator>().GetBool("Falling") == false)
        {
            GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 0);
            yield return new WaitForSeconds(.15f);
            GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 225);
            GetComponent<Animator>().SetBool("Hurt", false);
            for (int i = 0; i < 4; i++)
            {
                yield return new WaitForSeconds(.15f);
                GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 0);
                yield return new WaitForSeconds(.15f);
                GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 225);
            }
        }
        else
        {
            GetComponent<Animator>().SetBool("Hurt", false);
            for(int i = 0; i < 3; i++)
            {
                GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 0);
                yield return new WaitForSeconds(.15f);
                GetComponent<SpriteRenderer>().color = new Color(225, 225, 225, 225);
                yield return new WaitForSeconds(.15f);
            }
        }
        iFrames = false;
    }
}
