﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/ActionResponse/ChangeRoom")]
public class ChangeRoomResponse : ActionResponse
{
    public TextRoom roomToChangeTo;

    public override bool DoActionResponse(TextController controller)
    {
        if(controller.roomNavi.currentRoom.roomName == requiredString)
        {
            controller.roomNavi.currentRoom = roomToChangeTo;
            controller.DisplayRoomText();
            return true;
        }

        return false;
    }
}
