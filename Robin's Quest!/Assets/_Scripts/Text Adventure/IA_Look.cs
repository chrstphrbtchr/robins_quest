﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Look")]
public class IA_Look : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.DisplayRoomText();
    }
}
