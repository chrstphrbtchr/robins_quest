﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomNavigation : MonoBehaviour
{
    public TextRoom currentRoom;
    Dictionary<string, TextRoom> exitDictionary = new Dictionary<string, TextRoom>();
    TextController controller;

    private void Awake()
    {
        controller = GetComponent<TextController>();
    }

    public void UnpackExits()
    {
        for (int i = 0; i < currentRoom.exits.Length; i++)
        {
            exitDictionary.Add(currentRoom.exits[i].keyString, currentRoom.exits[i].valueRoom);
            controller.txtInteractions.Add(currentRoom.exits[i].exitDescription);
        }
    }

    public void AttemptChange(string dirNoun)
    {
        if (exitDictionary.ContainsKey(dirNoun))
        {
            currentRoom = exitDictionary[dirNoun];
            controller.LogStringWithReturn("You head off to the " + dirNoun);
            controller.DisplayRoomText();
        }
        else
        {
            controller.LogStringWithReturn("There is no path to the " + dirNoun);
            controller.DisplayRoomText();
        }
    }

    public void ClearExits()
    {
        exitDictionary.Clear();

    }
}
