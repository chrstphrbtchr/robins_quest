﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Inventory")]
public class TxtInventory : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.interactableItems.DisplayInventory();
    }
}
