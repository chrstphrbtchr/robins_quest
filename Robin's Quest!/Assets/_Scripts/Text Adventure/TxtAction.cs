﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TxtAction 
{
    // all this does is hold data
    // display the variables in the
    // inspector in a foldout.

    public InputAction inputAction;
    [TextArea]
    public string textResponse;
    // if the obj. gives a re: when examined,
    // give the textResponse.
    public ActionResponse actRe;
}
