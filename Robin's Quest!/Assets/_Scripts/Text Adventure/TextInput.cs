﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour
{
    public InputField inputField;
    TextController controller;

    private void Awake()
    {
        controller = GetComponent<TextController>();
        inputField.onEndEdit.AddListener(AcceptStringInput);
    }
    void AcceptStringInput(string userInput)
    {
        userInput = userInput.ToUpper();
        controller.LogStringWithReturn(userInput);

        char[] delimiterChars = { ' ' };
        // just a space! We're looking for a space
        //      that is separating our words.
        string[] sepInputWords = userInput.Split(delimiterChars);
        // take what is typed. look for spaces. 
        // separate into strings in our array of words.

        for (int i = 0; i < controller.inputActions.Length; i++)
        {
            InputAction inputAct = controller.inputActions[i];
            if(inputAct.keyword == sepInputWords[0])
            {
                inputAct.RespondToInput(controller, sepInputWords);  
            }
        }

        InputComplete();
    }
    void InputComplete()
    {
        controller.DisplayLog();
        inputField.ActivateInputField();
        inputField.text = null;
    }
}
