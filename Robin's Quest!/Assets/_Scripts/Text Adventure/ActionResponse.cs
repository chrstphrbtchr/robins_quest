﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionResponse : ScriptableObject
{
    public string requiredString;
    // the key to check against.

    public abstract bool DoActionResponse(TextController controller);
}
