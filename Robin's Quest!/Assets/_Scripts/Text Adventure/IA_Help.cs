﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Help")]
public class IA_Help : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.LogStringWithReturn("Type your responses in a [VERB] [NOUN] style, \n" +
            "such as 'TAKE SWORD' or 'GO NORTH'");
    }
}
