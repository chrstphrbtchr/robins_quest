﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Examine")]
public class IA_Examine : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.LogStringWithReturn(controller.
            TestVerbDictWithNoun(controller.interactableItems.examineDictionary, 
            sepInputWords[0], sepInputWords[1]));
    }
}

