﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Go")]
public class IA_Go : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.roomNavi.AttemptChange(sepInputWords[1]);
        // such as "Go [NORTH]" or "take [SKULL]"
    }
}
