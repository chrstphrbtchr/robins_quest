﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "TextAdventure/Room")]

public class TextRoom : ScriptableObject
{
    // store data & execute code!
    [TextArea] // allows to display as bigger text entry box in inspector
    public string description;
    public string roomName;
    public TextExit[] exits;
    public TextItems[] interactableObjectsInRoom;

}
