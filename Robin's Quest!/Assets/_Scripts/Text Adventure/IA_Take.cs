﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Take")]
public class IA_Take : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        Dictionary<string, string> takeDictionary = controller.interactableItems.Take(sepInputWords);
        if(takeDictionary != null)
        {
            controller.LogStringWithReturn(controller.
                TestVerbDictWithNoun(takeDictionary, 
                sepInputWords[0], sepInputWords[1]));
        }
    }
}
