﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextExit
{
    public string keyString;
    public string exitDescription;
    public TextRoom valueRoom;
}
