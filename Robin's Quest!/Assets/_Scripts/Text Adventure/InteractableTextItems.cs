﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableTextItems : MonoBehaviour
{
    public List<TextItems> usableItemsList;
    public Dictionary<string, string> examineDictionary = new Dictionary<string, string>();
    public Dictionary<string, string> takeDictionary = new Dictionary<string, string>();
    [HideInInspector] public List<string> nounsInRoom = new List<string>();
    Dictionary<string, ActionResponse> useDictionary = new Dictionary<string, ActionResponse>();
    List<string> inventoryNouns = new List<string>();
    TextController controller;

    private void Awake()
    {
        controller = GetComponent<TextController>();
    }
    public string GetObjectsNotInInventory(TextRoom currentRoom, int i)
    {
        TextItems interactableInRoom = currentRoom.interactableObjectsInRoom[i];

        if (!inventoryNouns.Contains(interactableInRoom.noun))
        {
            nounsInRoom.Add(interactableInRoom.noun);
            return interactableInRoom.description;
        }
        else
        {
            return null;
        }
    }

    public void AddActionResponsesToUseDictionary()
    {
        for (int i = 0; i < inventoryNouns.Count; i++)
        {
            string noun = inventoryNouns[i];

            TextItems intObjInInventory = GetInteractableObjFromUsableList(noun);
            // get me the object from our list of obj. using the name
            if (intObjInInventory == null)
                continue;
            for (int j = 0; j < intObjInInventory.interactions.Length; j++)
            {
                TxtAction interact = intObjInInventory.interactions[j];
                if (interact.actRe == null)
                    continue;
                if (!useDictionary.ContainsKey(noun))
                {
                    // if not in use dict & passed other tests...
                    useDictionary.Add(noun, interact.actRe);
                    // pass in a noun, get an action response.
                }
            }
        }

    }

    TextItems GetInteractableObjFromUsableList(string noun)
    {
        for (int i = 0; i < usableItemsList.Count; i++)
        {
            if(usableItemsList[i].noun == noun)
            {
                return usableItemsList[i];
            }
        }
        return null;
    }

    public void DisplayInventory()
    {
        controller.LogStringWithReturn("You look in your backpack. \nInside your backpack you have: ");
        for (int i = 0; i < inventoryNouns.Count; i++)
        {
            controller.LogStringWithReturn(inventoryNouns[i]);
        }
    }

    public void ClearCollections()
    {
        examineDictionary.Clear();
        nounsInRoom.Clear();
        takeDictionary.Clear();
    }

    public Dictionary<string, string> Take (string[] sepInputWords)
    {
        string noun = sepInputWords[1];
        if (nounsInRoom.Contains(noun))
        {
            inventoryNouns.Add(noun);
            AddActionResponsesToUseDictionary();
            nounsInRoom.Remove(noun);
            return takeDictionary;
        }
        else
        {
            controller.LogStringWithReturn("There is no " + noun + " here...");
            return null;
        }
    }

    public void UseItem(string[] sepInputWords)
    {
        string nounToUse = sepInputWords[1];
        // the noun in our grammar
        if (inventoryNouns.Contains(nounToUse))
        {
            if (useDictionary.ContainsKey(nounToUse))
            {
                bool actionResult =
                    useDictionary[nounToUse].DoActionResponse(controller);
                if (!actionResult)
                {
                    controller.LogStringWithReturn("Hmm... Nothing happened.");
                }
            }
            else
            {
                controller.LogStringWithReturn("You can't use the " + nounToUse);
            }
        }
        else
        {
            controller.LogStringWithReturn("There is no " + nounToUse + " in your inventory.");
        }
    }
}
