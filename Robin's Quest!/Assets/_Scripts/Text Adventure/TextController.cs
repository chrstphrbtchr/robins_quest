﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public Text displayTxt;
    public InputAction[] inputActions;
    [HideInInspector] public RoomNavigation roomNavi;
    [HideInInspector] public List<string> txtInteractions = new List<string>();
    [HideInInspector] public InteractableTextItems interactableItems;
    List<string> actionLog = new List<string>();
    

    void Awake()
    {
        interactableItems = GetComponent<InteractableTextItems>();
        roomNavi = GetComponent<RoomNavigation>();
    }

    private void Start()
    {
        DisplayRoomText();
        DisplayLog();
    }

    public void DisplayRoomText()
    {
        ClearCollectionsForNewRoom();
        UnpackRoom();

        string joinedInteractions = string.Join("\n", txtInteractions.ToArray());

        string combinedTxt = 
            roomNavi.currentRoom.description + "\n" + joinedInteractions;

        LogStringWithReturn(combinedTxt);
    }

    void UnpackRoom()
    {
        roomNavi.UnpackExits();
        PrepObjects(roomNavi.currentRoom);
    }

    void PrepObjects(TextRoom currentRoom)
    {
        for (int i = 0; i < currentRoom.interactableObjectsInRoom.Length; i++)
        {
            string descriptNonInventory =
                interactableItems.GetObjectsNotInInventory(currentRoom, i);
            if(descriptNonInventory != null)
            {
                txtInteractions.Add(descriptNonInventory);
            }

            TextItems interactiveInRoom = currentRoom.interactableObjectsInRoom[i];

            for (int j = 0; j < interactiveInRoom.interactions.Length; j++)
            {
                TxtAction interaction = interactiveInRoom.interactions[j];

                if(interaction.inputAction.keyword == "EXAMINE")
                {
                    interactableItems.examineDictionary.Add(interactiveInRoom.noun, 
                        interaction.textResponse);
                }

                if (interaction.inputAction.keyword == "TAKE")
                {
                    interactableItems.takeDictionary.Add(interactiveInRoom.noun,
                        interaction.textResponse);
                }
            }
        }
    }

    public string TestVerbDictWithNoun(Dictionary<string, string> verbDict, 
        string verbage, string nounage) 
    {
        if (verbDict.ContainsKey(nounage))
        {
            return verbDict[nounage];
        }
        else
        {
            return "You cannot " + verbage + " the " + nounage;
        }
    }

    void ClearCollectionsForNewRoom() // <--------- find this.
    {
        interactableItems.ClearCollections();
        txtInteractions.Clear();
        roomNavi.ClearExits();
    }

    public void LogStringWithReturn(string stringToAdd)
    {
        actionLog.Add(stringToAdd + "\n");
    }

    public void DisplayLog()
    {
        string logAsTxt = string.Join("\n", actionLog.ToArray());

        displayTxt.text = logAsTxt;
    }
}
