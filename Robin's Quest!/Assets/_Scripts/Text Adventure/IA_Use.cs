﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Use")]
public class IA_Use : InputAction
{
    public override void RespondToInput(TextController controller, string[] sepInputWords)
    {
        controller.interactableItems.UseItem(sepInputWords);
    }
}
