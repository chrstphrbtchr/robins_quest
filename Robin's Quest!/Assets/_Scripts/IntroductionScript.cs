﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;
using FMOD;
using FMODUnity;

public class IntroductionScript : MonoBehaviour
{
    public Image titleCard;
    public Sprite princessIdle;
    public Text placeholder; // <--------
    public Text logoName;
    public CanvasGroup gb;
    public GameObject princess;
    public StudioEventEmitter ding;
    public StudioEventEmitter robintro;
    public bool introduced;
    bool ding_played; // ugh it's been a year since I wrote this script.

    void Update()
    {
        if (introduced)
        {
            if (Vector3.Distance(transform.position, new Vector3(0f,0f,-10f)) > 0)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                    new Vector3(0f, 0f, -10f), 2f * Time.deltaTime);
            }
            else
            {
                if(Vector3.Distance(princess.transform.localPosition,
                    new Vector3(1.25f, -3f, 0f)) > 0)
                {
                    princess.transform.localPosition =
                        Vector3.MoveTowards(princess.transform.localPosition,
                        new Vector3(1.25f, -3f, 0f), 0.75f * Time.deltaTime);
                }
                else
                {
                    princess.GetComponent<Animator>().enabled = false;
                    princess.GetComponent<SpriteRenderer>().sprite = princessIdle;
                    if(introduced && !placeholder.enabled)
                    {
                        Invoke("ShowTitle", 2.66f);
                    }
                    
                }
            }
        }
        else
        {
            if(logoName.transform.localPosition.y > 0)
            {
                logoName.transform.localPosition =
                    Vector3.MoveTowards(logoName.transform.localPosition,
                    Vector3.zero, 100 * Time.deltaTime);
            }
            else
            {
                if (!ding_played)
                {
                    ding.Play();
                    ding_played = true;
                }
                
                if (gb.alpha > 0)
                {
                    gb.alpha -= (0.75f * Time.deltaTime);
                }
                else
                {
                    Invoke("PlayRobintroduction", 0.25f);
                }
                
            }
        }
    }

    void PlayRobintroduction()
    {
        robintro.Play();
        introduced = true;
    }

    void ShowTitle()
    {
        placeholder.enabled = true;
    }
}
