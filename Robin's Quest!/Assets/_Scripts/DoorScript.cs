﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class DoorScript : MonoBehaviour
{
    BoxCollider2D[] kids;

    void Start()
    {
        kids = GetComponentsInChildren<BoxCollider2D>(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            foreach (BoxCollider2D b in kids)
            {
                b.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            foreach (BoxCollider2D b in kids)
            {
                b.gameObject.SetActive(true);
            }
        }
    }
}
