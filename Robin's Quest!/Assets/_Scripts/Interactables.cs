﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Interactables : MonoBehaviour
{
    public CapsuleCollider2D player;
    bool interacting;

    void Update()
    {
        if ((interacting) &&
            (Input.GetKeyDown(KeyCode.RightShift) ||
            Input.GetKeyDown(KeyCode.LeftShift)))
        {
            KonMari(); 
            // THIS DOES NOT
            // SPARK JOY...
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision == player)
        {
            interacting = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == player)
        {
            interacting = false;
        }
    }

    void KonMari()
    {
        if ((PlayerScript.equipped == PlayerScript.Items.Hammer)
            && this.tag == "Rock")
        {
            Destroy(this.gameObject);
        }
        else if ((PlayerScript.equipped == PlayerScript.Items.Shovel)
            && this.tag == "Tree")
        {
            Destroy(this.gameObject);
        }
        else
        {
            
        }
    }
}
