﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// THIS IS THE OLD ENEMY SCRIPT!!!
/// DELETE ME??????????????????????
/// </summary>
public class EnemyScript : MonoBehaviour
{
    /*
    // stats
    public int hp;
    float dmg;
    float speed;
    float waitTime;
    float deathTime;
    float playerDistance;
    string baddietype;

    // states
    bool attacking;
    bool idle;
    bool walking;
    bool blocked;
    public bool damaged;
    public bool falling;
    public bool swimming;
    enum EnemyStatus { ATTACKING, DAMAGED, DYING, FALLING, IDLE, SWIMMING, WALKING };

    // objects
    public GameObject ammo;
    public GameObject hometown;
    public static List<GameObject> baddies = new List<GameObject>() { };

    // animation
    Animator anim;

    // AI
    public string dir;
    Transform target; 
    Ray eyeline;
    Vector3 lookDir;
    RaycastHit2D[] hits;
    Vector2 nextMove;
    Vector2 moveAction;

    // items
    public bool hasKey;
    public bool hasBossKey;
    public bool hasPotion;
    public GameObject keyfab;
    public GameObject bosskeyfab;
    public GameObject botion;

    // BIGBLOB
    public GameObject blobPrefab; // for boss # 1
    public int phase;

    void Awake()
    {
        lookDir = Vector3.zero;
        hits = Physics2D.RaycastAll(transform.position, lookDir, 1);
        baddies.Add(this.gameObject);
        nextMove = Vector3.zero;
        waitTime = UnityEngine.Random.Range(2f, 4f);
        playerDistance = 0f;
        speed = 3f;
        anim = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        eyeline = new Ray(transform.position, target.position);
        hits = Physics2D.RaycastAll(transform.position, lookDir, 1);

        // movement and direction --------------------------------------------------
        if (target.transform.position.y > transform.position.y)
        {
            if (target.transform.position.x > transform.position.x)
            {
                if ((target.transform.position.x - transform.position.x) >
                    (target.transform.position.y - transform.position.y))
                {
                    dir = "E";
                    nextMove = Vector2.right;
                    lookDir = transform.right;
                    anim.SetBool("East", true);
                    anim.SetBool("North", false);
                }
                else
                {
                    dir = "N";
                    nextMove = Vector2.up;
                    lookDir = transform.up;
                    anim.SetBool("East", false);
                    anim.SetBool("North", true);
                }

                anim.SetBool("South", false);
                anim.SetBool("West", false);
            }
            else if (target.transform.position.x < transform.position.x)
            {
                if ((transform.position.x - target.transform.position.x) >
                    (target.transform.position.y - transform.position.y))
                {
                    dir = "W";
                    nextMove = Vector2.left;
                    lookDir = (-1 * transform.right);
                    anim.SetBool("West", true);
                    anim.SetBool("North", false);
                }
                else
                {
                    dir = "N";
                    nextMove = Vector2.up;
                    lookDir = transform.up;
                    anim.SetBool("West", false);
                    anim.SetBool("North", true);
                }
                anim.SetBool("South", false);
                anim.SetBool("East", false);
            }
            else
            {
                dir = "N";
                nextMove = Vector2.up;
                anim.SetBool("North", true);
                anim.SetBool("South", false);
                anim.SetBool("East", false);
                anim.SetBool("West", false);
            }
        }
        else if (target.transform.position.y < transform.position.y)
        {
            if (target.transform.position.x > transform.position.x)
            {
                if ((target.transform.position.x - transform.position.x) >
                    (transform.position.y - target.transform.position.y))
                {
                    dir = "E";
                    nextMove = Vector2.right;
                    lookDir = transform.right;
                    anim.SetBool("South", false);
                    anim.SetBool("East", true);
                }
                else
                {
                    dir = "S";
                    nextMove = Vector2.down;
                    lookDir = (-1 * transform.up);
                    anim.SetBool("South", true);
                    anim.SetBool("East", false);

                }
                anim.SetBool("West", false);
                anim.SetBool("North", false);
            }
            else if (target.transform.position.x < transform.position.x)
            {
                if ((transform.position.x - target.transform.position.x) >
                    (transform.position.y - target.transform.position.y))
                {
                    dir = "W";
                    nextMove = Vector2.left;
                    lookDir = (-1 * transform.right);
                    anim.SetBool("South", false);
                    anim.SetBool("West", true);
                }
                else
                {
                    dir = "S";
                    nextMove = Vector2.down;
                    lookDir = (-1 * transform.up);
                    anim.SetBool("South", true);
                    anim.SetBool("West", false);
                }
                anim.SetBool("North", false);
                anim.SetBool("East", false);
            }
            else
            {
                dir = "S";
                nextMove = Vector2.down;
                lookDir = (-1 * transform.up);
                anim.SetBool("North", false);
                anim.SetBool("South", true);
                anim.SetBool("East", false);
                anim.SetBool("West", false);
            }
        }
        else
        {
            if (target.transform.position.x > transform.position.x)
            {
                dir = "E";
                nextMove = Vector2.right;
                lookDir = transform.right;
                anim.SetBool("North", false);
                anim.SetBool("South", false);
                anim.SetBool("East", true);
                anim.SetBool("West", false);
            }
            else if (target.transform.position.x < transform.position.x)
            {
                dir = "W";
                nextMove = Vector2.left;
                lookDir = (-1 * transform.right);
                anim.SetBool("North", false);
                anim.SetBool("South", false);
                anim.SetBool("East", false);
                anim.SetBool("West", true);
            }
            else
            {
                dir = "S";
                nextMove = Vector2.down;
                lookDir = (-1 * transform.up);
                anim.SetBool("North", false);
                anim.SetBool("South", true);
                anim.SetBool("East", false);
                anim.SetBool("West", false);
            }
        }
        // end movement and direction ----------------------------------------------
 
        anim.SetFloat("HP", (hp * 1.0f));
        anim.SetBool("Attacking", attacking);
        anim.SetBool("Damaged", damaged);
        anim.SetBool("Walking", walking);

        foreach (RaycastHit2D r in hits)
        {
            if ((r.collider.tag == "Walls") ||
                (r.collider.tag == "Water") ||
                (r.collider.tag == "Pit") ||
                (r.collider.tag == "Enemy"))
            {
                blocked = true;
            }
            else
            {
                blocked = false;
            }
        }

        switch (baddietype)
        {
            case "WIZARD":
                break;
            case "ICEWIZARD":
                break;
            case "BLOB":
                break;
            case "NEGABLOB":
                break;
            case "MINOTAUR":
                break;
            default:
                break;
        }
    }

    public void StatBlock(string n)
    {
        baddietype = n;

        switch (n)
        {
            case "WIZARD":
                hp = 3;
                dmg = 1.5f;
                playerDistance = 1f;
                deathTime = 1f;
                StartCoroutine(WizardBehavior(EnemyStatus.IDLE));
                break;
            case "ICEWIZARD":
                hp = 3;
                dmg = 1.5f;
                playerDistance = 1f;
                break;
            case "BLOB":
                hp = 1;
                dmg = 1f;
                deathTime = 0.75f;
                StartCoroutine(BlobBehavior(EnemyStatus.IDLE));
                break;
            case "NEGABLOB":
                break;
            case "MINOTAUR":
                break;
            case "BIGBLOB":
                hp = phase;
                dmg = 1f;
                deathTime = 0.1f;
                break;
            default:
                Debug.LogError(n + " does not exist?");
                break;
        }
    }

    public void Projectile(string d)
    {
        GameObject bullet = Instantiate(ammo, transform.position, transform.rotation);

        switch (baddietype)
        {
            case "WIZARD":
                // instantiate reg. fireball going dir. d
                bullet.GetComponent<Projectiles>().CastSpell("FIRE", d);
                attacking = true;
                break;
            case "ICEWIZARD":
                break;
            default:
                Debug.LogError("A.F.K.");
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemies")
        {
            //collision.collider.GetComponent<Rigidbody2D>().AddRelativeForce()
        }
        else if (collision.collider.tag == "Player")
        {
            collision.collider.GetComponent<Rigidbody2D>().AddForce(nextMove * 2, 
                ForceMode2D.Impulse);
            collision.collider.GetComponent<PlayerScript>().TakeDamage(dmg);
        }
        else if (collision.collider.tag == "Movable")
        {

        }
        else if (collision.collider.tag == "Walls")
        {

        }
        else if ((collision.collider.tag == "Pits") ||
            (collision.collider.tag == "Water"))
        {

        }
        
        // spikes, player, water, pit, sworderang (bounce back)
        // movables that are currently moving
        // doors that shut?
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "Sword") 
            && (hp > 0))
        {
            GetComponent<Rigidbody2D>().AddForce(moveAction * -1f, ForceMode2D.Impulse);
            //Vector3 diff = (transform.position - collision.transform.position) / 2f;
            //transform.position =
            //    new Vector3(transform.position.x + diff.x,
            //    transform.position.y + diff.y, transform.position.z);
        }
    }

    public void Ouch(int hit)
    {
        if(baddietype != "")
        {
            if (hp > hit)
            {
                if (!damaged)
                {
                    hp = hp - hit;
                    damaged = true;
                    FindObjectOfType<SwordScript>().ReturnSword();
                    attacking = false;
                    walking = false;
                    idle = true;
                    switch (baddietype)
                    {
                        case "WIZARD":
                            StopAllCoroutines();
                            StartCoroutine(WizardBehavior(EnemyStatus.DAMAGED));
                            break;
                        case "BIGBLOB":
                            GetComponent<Animator>().SetBool("Damaged", true);
                            StopAllCoroutines();
                            StartCoroutine(BlobBehavior(EnemyStatus.DAMAGED));
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                // GO KICK ROCKS & DIE (WOO)
                hp = hp - hit;
                StopAllCoroutines();
                GetComponent<CapsuleCollider2D>().enabled = !GetComponent<CapsuleCollider2D>().enabled;
                attacking = false;
                damaged = false;
                walking = false;

                switch (baddietype)
                {
                    case "BIGBLOB":
                        StartCoroutine(BigBlob(phase - 1));
                        break;
                    default:
                        break;
                }

                baddies.Remove(this.gameObject);

                if (hasBossKey || hasKey || hasPotion)
                {
                    ItemDrop();
                }

                Destroy(this.gameObject, deathTime);
            }
        }
    }

    IEnumerator WizardBehavior(EnemyStatus e)
    {
        while (e == EnemyStatus.IDLE)
        {
            idle = true;
            yield return new WaitForSecondsRealtime(waitTime);
            moveAction = new Vector2(transform.position.x + nextMove.x, 
                transform.position.y + nextMove.y);

            foreach(RaycastHit2D r in hits)
            {
                if (blocked)
                {
                    e = EnemyStatus.IDLE;
                }
                else
                {
                    e = EnemyStatus.WALKING;
                    idle = false;
                }
            }
        }
        while (e == EnemyStatus.WALKING)
        {
            walking = true;
            if ((Vector2.Distance(transform.position, moveAction) > 0)
                && (!blocked))
            {
                transform.position = Vector2.MoveTowards(transform.position,
                    moveAction, speed * Time.deltaTime);
                yield return null;
            }
            else
            {
                walking = false;
                idle = true;
                waitTime = UnityEngine.Random.Range(1f, 4f);
                yield return new WaitForSecondsRealtime(waitTime);
                if (!blocked)
                {
                    Projectile(dir);
                    e = EnemyStatus.ATTACKING;
                }
                else
                {
                    StopAllCoroutines();
                    StartCoroutine(WizardBehavior(EnemyStatus.IDLE));
                }
                yield return null;
            }
        }
        while (e == EnemyStatus.ATTACKING)
        {
            yield return new WaitForSeconds(.5f); // casting time.
            waitTime = UnityEngine.Random.Range(1f, 4f);
            attacking = false;
            StopAllCoroutines();
            StartCoroutine(WizardBehavior(EnemyStatus.IDLE));
            yield return null;
        }
        while(e == EnemyStatus.DAMAGED)
        {
            yield return new WaitForSeconds(.25f);
            damaged = false;
            waitTime = UnityEngine.Random.Range(.5f, 2.5f);
            StopAllCoroutines();
            StartCoroutine(WizardBehavior(EnemyStatus.IDLE));
            yield return null;
        }
        yield return null;
    }

    IEnumerator BlobBehavior(EnemyStatus e)
    {
        if(e == EnemyStatus.IDLE)
        {
            idle = true;
            yield return new WaitForSecondsRealtime(waitTime);
            e = EnemyStatus.WALKING;
            idle = false;
            walking = true;
        }

        if(e == EnemyStatus.WALKING)
        {
            while(walking)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                    target.transform.position, (speed/1.5f) * Time.deltaTime);
                yield return null;
            }
            idle = true;
            waitTime = UnityEngine.Random.Range(0.5f, 3f);
            StartCoroutine(BlobBehavior(EnemyStatus.IDLE));
            yield return null;
        }

        while (e == EnemyStatus.DAMAGED)
        {
            yield return new WaitForSeconds(.25f);
            damaged = false;
            StartCoroutine(BlobBehavior(EnemyStatus.IDLE));
            GetComponent<Animator>().SetBool("Damaged", false);
            yield return null;
        }
    }

    public IEnumerator BigBlob(int b)
    {
        transform.localScale = new Vector3(b, b, 1);
        phase = b;

        if(b == 5)
        {
            // grand entrance
            PlayerMovement.frozen = true;
            yield return new WaitForSeconds(2);
            while(Vector3.Distance(transform.position, 
                new Vector3(-156.5f, 20.5f, 0f)) > 0)
            {
                transform.position =
                    Vector3.MoveTowards(transform.position,
                    new Vector3(-156.5f, 20.5f, 0f), 0.25f);
                yield return null;
            }
            // play land sound
            speed = speed / b;
            // play boss sting
            StartCoroutine(BlobBehavior(EnemyStatus.IDLE));
            // start boss music.
            PlayerMovement.frozen = false;
            FindObjectOfType<PlayerMovement>().rb.constraints = 
                RigidbodyConstraints2D.FreezeRotation;
            yield return null;
        }
        else
        {
            if(b > 0)
            {
                GameObject leftBlob =  Instantiate(blobPrefab,
                    new Vector3(transform.position.x - ((b + 1) / 2),
                    transform.position.y, transform.position.z), transform.rotation);
                leftBlob.GetComponent<EnemyScript>().phase = b;
                leftBlob.GetComponent<EnemyScript>().StatBlock("BIGBLOB");
                GameObject rightBlob = Instantiate(blobPrefab, 
                    new Vector3(transform.position.x + ((b + 1) / 2),
                    transform.position.y, transform.position.z), transform.rotation);
                rightBlob.GetComponent<EnemyScript>().phase = b;
                rightBlob.GetComponent<EnemyScript>().StatBlock("BIGBLOB");
                StartCoroutine(leftBlob.GetComponent<EnemyScript>().BlobBehavior(EnemyStatus.WALKING));
                StartCoroutine(rightBlob.GetComponent<EnemyScript>().BlobBehavior(EnemyStatus.WALKING));
            }
            else
            {
                if(baddies.Count <= 0)
                {
                    // play fanfare
                    ItemDrop();
                    // open door (once hammer get).
                }
                yield return null;
            }
        }
    }
    
    public void DestinationReached()
    {
        walking = false;
    }

    void ItemDrop()
    {
        if(baddies.Count <= 0)
        {
            if (hasBossKey)
            {
                
                if ((!falling) && (!swimming))
                {

                }
                else
                {

                }
                hometown.GetComponent<Rooms>().Locksmith();
            }
            else if (hasKey)
            {
                
                if ((!falling) && (!swimming))
                {
                    Instantiate(keyfab, this.transform.position, this.transform.rotation);
                }
                else
                {
                    Instantiate(keyfab, 
                        hometown.GetComponent<Rooms>().safetyNet, 
                        this.transform.rotation);
                }
                hometown.GetComponent<Rooms>().Locksmith();
            }
            else if (hasPotion)
            {
                if ((!falling) && (!swimming))
                {
                    
                }
                else
                {
                    // it's gone, baby!
                }
            }
            else
            {
                // item drops from bosses.
                switch (baddietype)
                {
                    case "BIGBLOB":
                        PickUps[] items = FindObjectsOfType<PickUps>();
                        foreach(PickUps p in items)
                        {
                            
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    */
}
