﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PickUps : MonoBehaviour
{
    public PlayerScript playerscript;
    public PauseScreen pauseScript;
    public BoxCollider2D player;
    Vector3 dropOff;
    bool playerInRange;
    string item_name;

    public Dialogue convo;
    public List<List<string>> listList = new List<List<string>>();

    // boss items
    public GameObject hammer;
    public GameObject shovel;
    public GameObject gem;
    public GameObject ring;

    void Start()
    {
        item_name = this.name;

        switch (item_name)
        {
            // DIALOGUE FOR ITEMS.
            case "Sword (Pick-Up)":
                List<string> SwordPickUp1 = new List<string>()
                {
                    "You got the <ci>Sworderang!</c>",
                    "Press <k > to throw it at",
                    "your <s>enemies</s>!"
                };
                listList.Add(SwordPickUp1); 
                break;
            case "Boots (Pick-Up)":
                break;
            case "Floppy Disc":
                List<string> FloppyPickUp = new List<string>()
                {
                    "You got the <c255100100255>??????????</c>!",
                    "Use it to <w>????????</w> and",
                    "to <s>?????????????</s>!!"
                };
                listList.Add(FloppyPickUp);
                break;  
            default:
                break;
        }
    }


    void Update()
    {
        if ((playerInRange) && Input.GetKeyDown(KeyCode.RightShift))
        {
            convo.StartConversation(listList);

            switch (item_name)
            {
                case "Sword (Pick-Up)":
                    // pick-up anim?
                    PlayerScript.sword = true;
                    break;
                case "Boots":
                    PlayerScript.boots = true;
                    pauseScript.bootsBW.SetActive(true);
                    break;
                case "Mushroom":
                    PlayerScript.mush = true;
                    pauseScript.mushBW.SetActive(true);
                    break;
                default:
                    Debug.LogError("No such item.");
                    break;
            }
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision == player)
        {
            playerInRange = true;
            switch (item_name)
            {
                case "Hammer":
                    PlayerScript.hammer = true;
                    Destroy(this.gameObject);
                    break;
                case "Shovel":
                    break;
                case "Gem":
                    break;
                case "Ring":
                    break;
                default:
                    break;
            }
        }
    } 

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == player)
        {
            playerInRange = false;
        }
    }    
}
