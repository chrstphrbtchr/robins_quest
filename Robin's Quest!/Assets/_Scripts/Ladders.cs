﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladders : MonoBehaviour
{
    // Welcome... to Ladders

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.transform.position = new Vector3(this.transform.position.x, 
                collision.transform.position.y, collision.transform.position.z);
            collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
            collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            collision.GetComponent<Animator>().SetBool("Climbing", true);
            collision.GetComponent<PlayerMovement>().move_speed = 3f;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Animator>().SetBool("Climbing", false);
            collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            collision.GetComponent<PlayerMovement>().move_speed = 5f;
        }
    }
}
