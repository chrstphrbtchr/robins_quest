﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Movables : MonoBehaviour
{
    public Vector3 startingPoint;
    public BoxCollider2D roomCollider;
    BoxCollider2D player;

    public bool chonk;
    public bool oneMove;
    public Vector2 destiny;
    bool hasMoved;
    bool isMoving;
    bool isDone;
    float timer;

    public GameObject[] doors;

    public GameObject[] itemDrop;

    void Start()
    {
        timer = 0;
        startingPoint = transform.localPosition;
        player = FindObjectOfType<PlayerScript>().GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (hasMoved)
        {
            if (Vector2.Distance(player.transform.position,
            roomCollider.transform.position) > 8f)
            {
                GetComponent<BoxCollider2D>().enabled = true;
                transform.localPosition = startingPoint;
                GetComponent<SpriteRenderer>().color = Color.white;
                transform.localScale = new Vector3(1, 1, 1);
                hasMoved = false;
            }

            if ((oneMove) && (!isDone))
            {
                if (Vector2.Distance(transform.localPosition, destiny) < 0.1f)
                {
                    foreach (GameObject g in doors)
                    {
                        g.SetActive(false);
                    }
                    foreach (GameObject k in itemDrop)
                    {
                        k.SetActive(true);
                    }
                    isDone = true;
                }
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Walls")
        {
            hasMoved = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.collider == player)
        {
            if((!chonk) && (!oneMove))
            {
                if (timer >= 1)
                {
                    collision.gameObject.GetComponent<Animator>().SetBool("Pushing", true);
                    hasMoved = true;
                    // move it!
                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezePositionY |
                            RigidbodyConstraints2D.FreezeRotation;
                        GetComponent<Rigidbody2D>().velocity = new Vector2(1, 0);
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezePositionY |
                            RigidbodyConstraints2D.FreezeRotation;
                        GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 0);
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezePositionX |
                            RigidbodyConstraints2D.FreezeRotation;
                        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1);
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezePositionX |
                            RigidbodyConstraints2D.FreezeRotation;
                        GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1);
                    }
                    else
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezeAll;
                        timer = 0;
                    }


                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                        GetComponent<Rigidbody2D>().constraints =
                            RigidbodyConstraints2D.FreezeAll;
                        timer = 0;
                    }
                }
                else
                {
                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }


                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }
                }
            }
            else if(chonk) // && (player.GetComponent<PlayerScript>().glove == true)
                // && (player.GetComponent<PlayerScript>().equipped == "GLOVE"))
            {
                if (!hasMoved && (timer >= 1))
                {
                    collision.gameObject.GetComponent<Animator>().SetBool("Pushing", true);

                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(1, 0, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(-1, 0, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(0, 1, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(0, -1, 0)));
                        }
                        timer = 0;
                    }
                    else
                    {

                    }

                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }
                }
                else if(!hasMoved)
                {
                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;

                    }

                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }
                }
                else
                {
                    // it moved, do nothing!
                }
            }
            else
            {
                if (!hasMoved && (timer >= 1))
                {
                    collision.gameObject.GetComponent<Animator>().SetBool("Pushing", true);

                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(1, 0, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(-1, 0, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(0, 1, 0)));
                        }
                        timer = 0;
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        if (!isMoving)
                        {
                            StartCoroutine(MoveBrick(new Vector3(0, -1, 0)));
                        }
                        timer = 0;
                    }
                    else
                    {
                        
                    }

                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }
                }
                else if (!hasMoved)
                {
                    if ((Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)) &&
                         (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)) &&
                        (Input.GetAxisRaw("Vertical") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else if ((Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)) &&
                        (Input.GetAxisRaw("Horizontal") == 0)))
                    {
                        timer += (2 * Time.deltaTime);
                    }
                    else
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }

                    if (Input.anyKeyDown)
                    {
                        collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
                        timer = 0;
                    }
                }
                else
                {
                    
                }
            }
        }
        else if(collision.collider.tag == "Enemies")
        {
            collision.collider.GetComponent<Rigidbody2D>().isKinematic = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.collider == player)
        {
            collision.gameObject.GetComponent<Animator>().SetBool("Pushing", false);
            if((!chonk) && (!oneMove))
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().constraints =
                    RigidbodyConstraints2D.FreezeAll;
            }
            timer = 0;
        }
        else if (collision.collider.tag == "Enemies")
        {
            collision.collider.GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Pits")
        {
            StartCoroutine(LostTile(collision.GetComponent<BoxCollider2D>()));
        }
    }

    IEnumerator MoveBrick(Vector3 v)
    {
        isMoving = true;
        GetComponent<Rigidbody2D>().constraints = 
            RigidbodyConstraints2D.FreezeRotation;
        while ((Vector3.Distance(transform.localPosition, startingPoint + v) > 0.1)
            && (!hasMoved))
        {
            player.GetComponent<Animator>().SetBool("Pushing", true);
            transform.localPosition = Vector3.MoveTowards(transform.localPosition,
                startingPoint + v, 0.025f);
            yield return null;
        }
        hasMoved = true;
        GetComponent<Rigidbody2D>().constraints =
                    RigidbodyConstraints2D.FreezeAll;
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        timer = 0;
        player.GetComponent<Animator>().SetBool("Pushing", false);
        isMoving = false;
        yield return null;  
    }

    IEnumerator LostTile(BoxCollider2D pit)
    {
        hasMoved = true;
        for (float i = 1; i > 0; i -= 0.2f)
        {
            transform.position =
                 Vector3.MoveTowards(transform.position,
                pit.transform.position, 0.33f);
            GetComponent<SpriteRenderer>().color =
                new Color(i, i, i, i);
            transform.localScale = (transform.localScale * i);
            yield return new WaitForSeconds(0.075f);
        }
        // splash or fall or whatever!
        GetComponent<BoxCollider2D>().enabled = false;
        yield return null;
    }
}
