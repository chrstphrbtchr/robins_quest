﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public PlayerScript ps;

    public GameObject[] empties;
    public Image[] pieces0;
    public Image[] pieces1;
    public Image[] pieces2;
    public Image[] pieces3;
    public Image[] pieces4;
    public Image[] pieces5;
    public Image[] pieces6;
    public Image[] pieces7;

    public List<Image[]> hearts;
    public List<Image> displayedHearts;

    public int maxHP;
    public float currentHP;
    int currentHeart;

    void Start()
    {
        hearts = new List<Image[]>
        {
            pieces0,
            pieces1,
            pieces2,
            pieces3,
            pieces4,
            pieces5,
            pieces6,
            pieces7
        };

        currentHeart = maxHP;
        maxHP = ps.maxHP;
        currentHP = maxHP;
        UpdateMaxHealth(maxHP);
    }

    public void UpdateHealth(float h)
    {
        currentHeart = Convert.ToInt32(h);

        float x = currentHP - h;

        for(int i = 0; x > i;)
        {
            displayedHearts[(displayedHearts.Count - 1)].enabled = false;
            displayedHearts.RemoveAt(displayedHearts.Count - 1);
            x -= 0.25f;
        }
        currentHP = ps.currentHP;
    }

    public void UpdateMaxHealth(int h)
    {
        maxHP = h;
        currentHeart = maxHP;
        currentHP = maxHP;
        ps.maxHP = maxHP;
        ps.currentHP = maxHP;

        for (int i = 0; i < h; i++)
        {
            empties[i].SetActive(true);
            foreach(Image p in hearts[i])
            {
                p.enabled = true;
                displayedHearts.Add(p);
            }
        }
    }

    public void Healing(float f)
    {
        if ((currentHP + f) < maxHP)
        {
            currentHP = currentHP + f;
        }
        else
        {
            currentHP = maxHP;
        }

        for(int i = 0; f > i;)
        {
            foreach (Image p in hearts[currentHeart])
            {
                if ((!p.enabled) && (f > 0))
                {
                    p.enabled = true;
                    displayedHearts.Add(p);
                    f = f - 0.25f;
                }
            }
            if((currentHeart + 1 <= maxHP) && (f > 0))
            {
                currentHeart = currentHeart + 1;
            }
        }

        ps.currentHP = currentHP;
    }

    public void GameOver()
    {

    }

}
